/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Collection de fonctions statiques d'appel Ajax
 * 
 * @namespace
 */
Scrutari.Ajax = {};

/**
 * Appel Ajax. Par défaut, fait appel à la fonction de JQuery $.ajax.
 *  * settings correspond à l'objet passé en argument àr [jQuery.ajax()]{@link https://api.jquery.com/jQuery.ajax/#jQuery-ajax-url-settings}
 * Pour ne pas utiliser JQuery, il faut remplacer cette fonction.
 * 
 * @param {Object} settings
 * @returns {undefined}
 */
Scrutari.Ajax.load = function (settings) {
    $.ajax(settings);
};


/**
 * Fonction de retour après chargement d'une requête
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_base type=base}.
 * 
 * @callback Scrutari.Ajax~loadBaseArrayCallback
 * @param {Scrutari~BaseApiObject[]} baseArray tableau des bases demandées
 * @returns {void}
 */

/**
 * @summary Requête Json type=base
 * @description
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_base}
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadBaseArrayCallback} baseArrayCallback
 * @returns {void}
 */
Scrutari.Ajax.loadBaseArray = function (api, requestParameters, baseArrayCallback) {
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("base", requestParameters),
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "baseArray", baseArrayCallback);
        }
    });
};

/**
 * Fonction de retour après chargement d'une requête
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_category type=category}.
 * 
 * @callback Scrutari.Ajax~loadCategoryArrayCallback
 * @param {Scrutari~CategoryApiObject[]} categoryArray
 * @returns {void}
 */

/**
 * @summary Requête Json type=category
 * @description
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_category}
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadCategoryArrayCallback} categoryArrayCallback
 * @returns {void}
 */
Scrutari.Ajax.loadCategoryArray = function (api, requestParameters, categoryArrayCallback) {
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("category", requestParameters),
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "categoryArray", categoryArrayCallback);
        }
    });
};

/**
 * Fonction de retour après chargement d'une requête
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_corpus type=corpus}.
 * 
 * @callback Scrutari.Ajax~loadCorpusArrayCallback
 * @param {Scrutari~CorpusApiObject[]} corpusArray
 * @returns {void}
 */

/**
 * @summary Requête Json type=corpus
 * @description
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_corpus}
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadCorpusArrayCallback} corpusArrayCallback
 * @returns {void}
 */
Scrutari.Ajax.loadCorpusArray = function (api, requestParameters, corpusArrayCallback) {
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("corpus", requestParameters),
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "corpusArray", corpusArrayCallback);
        }
    });
};

/**
 * Fonction de retour après chargement d'une requête
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_thesaurus type=thesaurus}.
 *  
 * @callback Scrutari.Ajax~loadThesaurusArrayCallback
 * @param {Scrutari~ThesaurusApiObject[]} thesaurusArray
 * @returns {void}
 */

/**
 * @summary Requête Json type=thesaurus
 * @description
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_thesaurus}
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadThesaurusArrayCallback} thesaurusArrayCallback
 * @returns {void}
 */
Scrutari.Ajax.loadThesaurusArray = function (api, requestParameters, thesaurusArrayCallback) {
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("thesaurus", requestParameters),
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "thesaurusArray", thesaurusArrayCallback);
        }
    });
};

/**
 * Fonction de retour après chargement d'une requête
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_engine type=engine}.
 *  
 * @callback Scrutari.Ajax~loadEngineInfoCallback
 * @param {Scrutari~EngineInfoApiObject} engineInfo Objet récupéré
 * @returns {void}
 */

/**
 * @summary Requête Json type=engine
 * @description
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_engine}
 *
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadEngineInfoCallback} engineInfoCallback
 * @returns {void}
 */
Scrutari.Ajax.loadEngineInfo = function (api, requestParameters, engineInfoCallback) {
    var defaultParameters = {
        "info": "all"
    };
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("engine", defaultParameters, requestParameters),
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "engineInfo", engineInfoCallback);
        }
    });
};

/**
 * Fonction de retour après chargement d'une requête
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_fiche type=fiche}.
 * 
 * @callback Scrutari.Ajax~loadFicheArrayCallback
 * @param {Scrutari~FicheApiObject[]} ficheArray
 * @returns {void}
 */

/**
 * @summary Requête Json type=fiche
 * @description
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_fiche}
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadFicheArrayCallback} ficheArrayCallback
 * @returns {void}
 */
Scrutari.Ajax.loadFicheArray = function (api, requestParameters, ficheArrayCallback) {
    var defaultParameters = {
        "fieldvariant": "data"
    };
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("fiche", defaultParameters, requestParameters),
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, ["ficheArray", "motcleArray"], ficheArrayCallback);
        }
    });
};

/**
 * Fonction de retour après chargement d'une requête
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_motcle type=motcle}. 
 *  
 * @callback Scrutari.Ajax~loadMotcleArrayCallback
 * @param {Scrutari~MotcleApiObject[]} motcleArray
 * @returns {void}
 */

/**
 * @summary Requête Json type=motcle
 * @description
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_motcle}
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadMotcleArrayCallback} motcleArrayCallback
 * @returns {void}
 */
Scrutari.Ajax.loadMotcleArray = function (api, requestParameters, motcleArrayCallback) {
    var defaultParameters = {
        "fieldvariant": "data"
    };
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("motcle", defaultParameters, requestParameters),
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "motcleArray", motcleArrayCallback);
        }
    });
};

/**
 * Fonction de retour après chargement d'une requête
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_geojson type=geojson}.
 * Les données retournées suivent le format {@link https://geojson.org/ GeoJson}.
 * 
 * @callback Scrutari.Ajax~loadGeoJsonCallback
 * @param {Object} data Objet au format GeoJson
 * @returns {void}
 */

/**
 * Charge des données au format GeoJson tel que défini dans http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_geojson
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadGeoJsonCallback} geojsonCallback
 * @param {Scrutari.Ajax~errorCallback} [errorCallback]
 * @returns {void}
 */
Scrutari.Ajax.loadGeoJson = function (api, requestParameters, geojsonCallback, errorCallback) {
     var defaultParameters = {
        "fieldvariant": api.options.queryVariant,
        "origin": api.origin
    };
    if (api.options.ficheFields !== null) {
        defaultParameters["fichefields"] = api.options.ficheFields;
    }
    if (api.options.motcleFields !== null) {
        defaultParameters["motclefields"] = api.options.motcleFields;
    }
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("geojson", defaultParameters, requestParameters),
        success: function (data, textStatus) {
            if (data.hasOwnProperty("error")) {
                if (errorCallback) {
                    errorCallback(data.error);
                } else {
                    Scrutari.logError(data.error);
                }
            } else {
                Scrutari.Ajax.logWarnings(data);
                geojsonCallback(data);
            }
        }
    });
};

/**
 * Fonction de retour après chargement d'une requête
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_qfiche type=q-fiche}.
 * 
 * @callback Scrutari.Ajax~loadFicheSearchResult
 * @param {Scrutari~FicheSearchResultApiObject} data Objet récupéré
 * @returns {void}
 */

/**
 * Lance une nouvelle recherche.
 * requestParameters est obligatoire et doit posséder une propriété q ou des propriétés q_*
 * tel que défini dans http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_qfiche
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadFicheSearchResult} ficheSearchResultCallback
 * @param {Scrutari.Ajax~errorCallback} [errorCallback]
 * @returns {void}
 */
Scrutari.Ajax.loadFicheSearchResult = function (api, requestParameters, ficheSearchResultCallback, errorCallback) {
    var defaultParameters = {
        "fieldvariant": api.options.queryVariant,
        "origin": api.origin,
        "start": 1,
        "limit": api.getLimit(),
        "starttype": "in_all",
        "q-mode": "intersection"
    };
    if (api.options.ficheFields !== null) {
        defaultParameters["fichefields"] = api.options.ficheFields;
    }
    if (api.options.motcleFields !== null) {
        defaultParameters["motclefields"] = api.options.motcleFields;
    }
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("q-fiche", defaultParameters, requestParameters),
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheSearchResult", ficheSearchResultCallback, errorCallback);
        }
    });
};

/**
 * Charge des résultats supplémentaires pour une recherche existante. 
 * requestParameters est obligatoire et doit contenir une propriété qid
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {Scrutari.Ajax~loadFicheSearchResult} existingFicheSearchResultCallback
 * @returns {void}
 */
Scrutari.Ajax.loadExistingFicheSearchResult = function (api, requestParameters, existingFicheSearchResultCallback) {
    var defaultParameters = {
        "fieldvariant": api.options.queryVariant,
        "insert": "-searchmeta,-motclearray",
        "start": 1,
        "limit": api.getLimit()
    };
    if (api.options.ficheFields !== null) {
        defaultParameters["fichefields"] = api.options.ficheFields;
    }
    if (api.options.motcleFields !== null) {
        defaultParameters["motclefields"] = api.options.motcleFields;
    }
    Scrutari.Ajax.load({
        url: api.getJsonUrl(),
        dataType: api.options.dataType,
        data: api.buildRequestParameters("q-fiche", defaultParameters, requestParameters),
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheSearchResult", existingFicheSearchResultCallback);
        }
    });
};

/**
 * Fonction invoquée lors du succès d'une requête Ajax, traite les messages d'erreur
 * (si errorCallback n'est pas défini, envoie dans la console))
 * et les messages d'avertissement avant de faire appel à la fonction objectCallback en envoyant
 * le bon objet.
 * 
 * @param {Object} ajaxResult objet retourné par la requête Ajax
 * @param {string|string[]} objectNames nom ou tableau de nom des propriétés désignant les objets à traiter
 * @param {function} objectCallback fonction de retour à appliquer aux objets désignés par objectNames
 * @param {errorCallback} [errorCallback]
 * @returns {void}
 * @private
 */
Scrutari.Ajax.success = function(ajaxResult, objectNames, objectCallback, errorCallback) {
    if (ajaxResult.hasOwnProperty("error")) {
        if (errorCallback) {
            errorCallback(ajaxResult.error);
        } else {
            Scrutari.logError(ajaxResult.error);
        }
    } else {
        Scrutari.Ajax.logWarnings(ajaxResult);
        if (!objectCallback) {
            return;
        }
        if (Array.isArray(objectNames)) {
            let apiObjectArray = new Array();
            for(let objectName of objectNames) {
                if (!ajaxResult.hasOwnProperty(objectName)) {
                    apiObjectArray.push(null);
                } else {
                    apiObjectArray.push(ajaxResult[objectName]);
                }
            }
            objectCallback.apply(this, apiObjectArray);
        } else {
            let objectName = objectNames;
            if (!ajaxResult.hasOwnProperty(objectName)) {
                throw new Error(objectName + " object is missing in json response");
            } else {
                objectCallback(ajaxResult[objectName]);
            }
        }
    }
};

/**
 * Teste la présence d'avertissements et les écrit dans le journal
 * 
 * @param {Object} ajaxResult objet retourné par la requête Ajax
 * @returns {void}
 * @private
 */
Scrutari.Ajax.logWarnings = function (ajaxResult) {
    if (ajaxResult.hasOwnProperty("warnings")) {
        var warningsMessage = "Scrutari Request Warnings [";
        for(let i = 0; i < ajaxResult.warnings.length; i++) {
            if (i > 0) {
                warningsMessage += ";";
            }
            let warning = ajaxResult.warnings[i];
            warningsMessage += "key = ";
            warningsMessage += warning.key;
            warningsMessage += " | parameter = ";
            warningsMessage += warning.parameter;
            if (warning.hasOwnProperty("value")) {
                warningsMessage += " | value = ";
                warningsMessage += warning.value;
            }
        }
        warningsMessage += "]";
        Scrutari.log(warningsMessage);
    }
};

/**
 * Fonction de retour en cas d'erreur indiquée par le moteur Scrutari.
 * 
 * @callback Scrutari.Ajax~errorCallback
 * @param {Scrutari~ErrorApiObject} error
 * @returns {void}
 */
