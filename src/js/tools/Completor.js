/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */


/**
 * @classdesc
 * Objet faisant office de collection de fonctions sur les objets de l'API
 * pour les compléter en rajoutant des propriétés. Ces propriétés commencent par un
 * souligné « _ ». Ces propriétés calculées permettent notamment d'écrire des gabarits
 * plus simples en évitant des tests de conditionnalité à l'intérieur du gabarit.
 * 
 * @class
 * @param {Scrutari.Meta} scrutariMeta
 */
Scrutari.Completor = function (scrutariMeta) {
    this.scrutariMeta = scrutariMeta;
};


/**
 * @summary Ajoute un tableau des mots-clés répartis par thésaurus
 * @description
 * La propriété bythesaurusMap de l'API ne contient que les codes des mots-clés et
 * thésaurus. Cette fonction rajoute à la fiche la propriété _bythesaurusArray qui est
 * un tableau d'objets possédant deux propriétés : thesaurus (une instance de ThesaurusApiObject tel
 * que renvoyé par scrutariMeta et motcleArray, le tableau des mots-clés).
 *  * 
 * @param {Scrutari~FicheApiObject} fiche fiche à compléter
 * @param {Scrutari~motcleProvider} motcleProvider 
 * @returns {boolean} true si un changement a été effectué
 */
Scrutari.Completor.prototype.bythesaurusArray = function (fiche, motcleProvider) {
    var scrutariMeta = this.scrutariMeta;
    if (!fiche.hasOwnProperty("bythesaurusMap")) {
        return false;
    }
    let thesaurusArray = scrutariMeta.getThesaurusArray();
    let _bythesaurusArray = new Array();
    for(let thesaurus of thesaurusArray) {
        let codethesaurus = thesaurus.codethesaurus;
        let key = "code_" + codethesaurus;
        if (fiche.bythesaurusMap.hasOwnProperty(key)) {
            let motcleArray = new Array();
            for(let codemotcle of fiche.bythesaurusMap[key]) {
                let motcle = motcleProvider(codemotcle);
                if (motcle) {
                    motcleArray.push(motcle);
                }
            }
            _bythesaurusArray.push({
                thesaurus: thesaurus,
                motcleArray: motcleArray
            });
        }
    }
    fiche._bythesaurusArray = _bythesaurusArray;
    return true;
};

/**
 *  Ajoute aux champs complémentaires une propriété title avec le titre du complément
 *  
 * @param {Scrutari~FicheApiObject} fiche fiche à compléter
 * @returns {boolean} true si un changement a été effectué
 */
Scrutari.Completor.prototype.complementTitle = function (fiche) {
    var scrutariMeta = this.scrutariMeta;
    if (fiche.hasOwnProperty("mcomplementArray")) {
        for(let mcomplement of fiche.mcomplementArray) {
            mcomplement.title = scrutariMeta.getComplementTitle(fiche.codecorpus, mcomplement.number);
        }
        return true;
    } else {
        return false;
    }
};

/**
 * Ajoute une propriété _icon basé sur la valeur de icon ou, à défaut, ficheicon
 * 
 * @param {Scrutari~FicheApiObject} fiche fiche à compléter
 * @returns {boolean} true si un changement a été effectué
 */
Scrutari.Completor.prototype.icon = function (fiche) {
    if (fiche.hasOwnProperty("icon")) {
        fiche._icon = fiche.icon;
        return true;
    } else if (fiche.hasOwnProperty("ficheicon")) {
        fiche._icon = fiche.ficheicon;
        return true;
    } else {
        return false;
    }
};

/**
 * @summary Organise les attributs marqués d'une fiche sous la forme d'un tableau d'attributs pour
 * une famille donnée.
 * @description
 * La propriété rajoutée a pour nom _{familleName}AttributeArray qui contient des objets
 * qui ont les propriétés : name (nom de l'attribute), title (libellé de l'attribut),
 * type (nom de la famille), valueArray (tableau des valeurs)
 * 
 * @param {Scrutari~FicheApiObject} fiche fiche à compléter
 * @param {string} familyName Nom de la famille d'attributs
 * @returns {boolean} true si un changement a été effectué
 */
Scrutari.Completor.prototype.markedAttributeArray = function (fiche, familyName ) {
    var scrutariMeta = this.scrutariMeta;
    let attributeArray = scrutariMeta.getAttributeArray(familyName);
    if (attributeArray.length === 0) {
        return false;
    }
    let objArray = new Array();
    for(let attribute of attributeArray) {
        if (fiche.mattrMap.hasOwnProperty(attribute.name)) {
            objArray.push({
                name: attribute.name,
                title: attribute.title,
                type: attribute.type,
                valueArray: fiche.mattrMap[attribute.name]
            });
        }
    }
    fiche["_" + familyName + "AttributeArray"] = objArray;
    return true;
};

/**
 * @summary Ajoute un tableau des mots-clés d'indexation
 * @description
 * L'API ne renvoie que le tableau des codes des mots-clés indexant la fiche.
 *  
 * @param {Scrutari~FicheApiObject} fiche fiche à compléter
 * @param {Scrutari~motcleProvider} motcleProvider 
 * @returns {boolean} true si un changement a été effectué
 */
Scrutari.Completor.prototype.motcleArray = function (fiche, motcleProvider) {
     if (!fiche.hasOwnProperty("codemotcleArray")) {
            return false;
    }
    let motcleArray = new Array();
    for(let codemotcle of fiche.codemotcleArray) {
        let motcle = motcleProvider(codemotcle);
        if (motcle) {
            motcleArray.push(motcle);
        }
    }
    if (motcleArray.length > 0) {
        fiche._motcleArray = motcleArray;
        return true;
    } else {
        return false;
    }
};

/**
 * Ajoute une propriété _target avec la valeur de target 
 * 
 * @param {Scrutari~FicheApiObject} fiche fiche à compléter
 * @param {string} target
 * @returns {boolean} retourne toujours true
 */
Scrutari.Completor.prototype.target = function (fiche, target) {
    fiche._target = target;
    return true;
};

/**
 * Ajoute une propriété _thumbnail (vignette de la fiche) en vérifiant la présence d'une propriété
 * thumbnail ou d'un attribut sct:thumbnail
 * 
 * @param {Scrutari~FicheApiObject} fiche fiche à compléter
 * @returns {boolean} true si un changement a été effectué
 */
Scrutari.Completor.prototype.thumbnail = function (fiche) {
    if (fiche.hasOwnProperty("thumbnail")) {
        fiche._thumbnail = fiche.thumbnail;
        return true;
    } else if (Scrutari.hasAttribute(fiche, "sct:thumbnail")) {
        fiche._thumbnail = fiche.attrMap["sct:thumbnail"][0];
        return true;
    } else {
        return false;
    }
};
