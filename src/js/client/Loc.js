/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Cosntructeur d'un objet Scrutari.Loc.
 * L'argument unique est un objet jouant le rôle de tableau associatif, les noms
 * de ses propriétés étant les clés de localisation les valeurs de ces propriétés
 * étant censées être des chaines.
 * 
 * @constructor
 * @param {Object} [map] objet jouant le rôle de tableau associatif
 */
Scrutari.Loc = function (map) {
    if (map) {
        this.map = map;
    } else {
        this.map = new Object();
    }
};

/**
 * Remplit le tableau de localisation avec les valeurs des propriétés de l'object
 * map (ces valeurs étant censées être des chaines).
 * Si la clé existe déjà, l'ancienne valeur est remplacée.
 *  
 * @param {Object} map objet jouant le rôle de tableau associatif
 * @returns {void}
 */
Scrutari.Loc.prototype.putAll = function (map) {
    for(var key in map) {
        this.map[key] = map[key];
    }
};

/**
 * Indique le texte de localisation  pour une clé donnée.
 * 
 * @param {string} locKey la clé de localisation
 * @param {string} locText le text de localisation
 * @returns {void}
 */
Scrutari.Loc.prototype.putLoc = function (locKey, locText) {
    this.map[locKey] = locText;
};

/**
 * Localise le texte indiqué par la clé de localisation.
 * Cette fonction peut prendre des valeurs supplémentaires qui seront placés
 * dans le texte résultant aux endroits indiqués sous la forme {0},{1}, etc.
 * (le chiffre indiquant le numéro de l'argument supplémentaire, en comptant à partir
 * de zéro)
 * 
 * @param {string} locKey la clé de localisation
 * @returns {string} le texte localisé
 */
Scrutari.Loc.prototype.loc = function (locKey, ...values) {
    if (!this.map.hasOwnProperty(locKey)) {
        return locKey;
    }
    var text = this.map[locKey];
    var length = values.length;
    if ((length === 1) && (Array.isArray(values[0]))) {
        values = values[0];
        length = values.length;
    }
    if (length > 0) {
        for(let i = 0; i < length; i++) {
            let mark = "{" + i + "}";
            text = text.replace(mark, values[i]);
        }
    }
    return text;
};

/**
 * Localise la clé et échappe le texte résultant.
 * Comme Scrutari.loc, cette fonction peut prendre des arguments supplémentaires
 * qui seront placés dans le texte résultant.
 * 
 * @param {string} locKey la clé de localisation
 * @returns {string} le texte localisé et échappé
 */
Scrutari.Loc.prototype.escape = function (locKey, ...values) {
    if ((values.length === 1) && (Array.isArray(values[0]))) {
        values = values[0];
    }
    return Scrutari.escape(this.loc(locKey, values));
};

