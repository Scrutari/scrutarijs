/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Objet argument du constructeur. Toute propriété de l'objet qui n'est pas dans la liste est traitée comme un option.
 * 
 * Autrement dit, une option peut être définie aussi bien comme une propriété directe de l'objet settings que comme propriété
 * de l'objet settings.options.
 * 
 * Si une propriété est présente dans les deux, c'est la propriété directe de settings qui l'emporte.
 * 
 * @typedef Scrutari.SearchUnit~settings
 * @type {Object}
 * @property {Scrutari.Api} api - Accès au moteur Scrutari
 * @property {Object} result - Objet ficheSearchResult de l'API SCrutari
 * @property {Object} requestParameters - paramètres envoyés à la requête Ajax
 * @property {function} [groupSortFunction] - Fonction optionnelle de tri des groupes
 * @property {string} searchOrigin - Paramètre origin transmis au moteur Scrutari
 * */

/**
 * @classdec
 * Objet encapsulant un objet ficheSearchResult obtenu via l'API Json (requête type=q-fiche) et proposant
 * des accesseurs et des méthodes de traitement.
 * Voir http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_qfiche
 * 
 * @class
 * @param {Scrutari.SearchUnit~settings} settings - Object argument
 * 
 * @property {Scrutari.Api} api - Accès au moteur Scrutari
 * @property {Object} ficheSearchResult - Objet ficheSearchResult de l'API SCrutari
 * @property {Object} requestParameters - paramètres envoyés à la requête Ajax
 * @property {searchMeta} searchMeta - Objet d'information sur la recherche
 * @property {string} searchOrigin - Paramètre origin transmis au moteur Scrutari
 *
 */
Scrutari.SearchUnit = function (settings) {
    var result = settings.ficheSearchResult;
    var completeFicheFunction = settings.completeFicheFunction;
    this.api = settings.api;
    this.ficheSearchResult = result;
    this.requestParameters = settings.requestParameters;
    this.searchMeta = result.searchMeta;
    this.ficheGroupArray = result.ficheGroupArray;
    this.completeFicheFunction = completeFicheFunction;
    this.searchOrigin = settings.searchOrigin;
    var groupSortFunction = settings.groupSortFunction;
    if ((groupSortFunction) && (this.ficheGroupArray.length > 1)) {
        this.ficheGroupArray = this.ficheGroupArray.sort(groupSortFunction);
    }
    this.motcleMap = new Object();
    if (result.hasOwnProperty("motcleArray")) {
        for(let motcle of result.motcleArray) {
            this.motcleMap["code_" + motcle.codemotcle] = motcle;
        }
    }
    this.notfoundArray = [];
    for(let reportItem of result.searchMeta.reportArray) {
        if (reportItem.canonicalArray.length === 0) {
            this.notfoundArray.push(reportItem);
        }
    }
    for(let group of this.ficheGroupArray) {
        let ficheGroupName;
        if (group.category){
            ficheGroupName = group.category.name;
        }
        for(let fiche of group.ficheArray) {
            completeFicheFunction(this, fiche, ficheGroupName);
        }
    }
    
};

/**
 * Retourne l'identifiant de la recherche au sein du moteur Scrutari.
 * 
 * @returns {string} Identifiant de la recherche
 */
Scrutari.SearchUnit.prototype.getQId = function () {
    if (this.searchMeta) {
        return this.searchMeta.qId;
    } else {
        return "";
    }
};

/**
 * Retourne la séquence de recherche originale
 * 
 * @returns {string} Séquence de la recherche
 */
Scrutari.SearchUnit.prototype.getQ = function () {
    if (this.searchMeta) {
        return this.searchMeta.q;
    } else {
        return "";
    }
};

/**
 * @summary Retourne le nombre de fiches comprises dans le résultat de la recherche
 * @description
 * Si ficheGroupName n'est pas renseigné, renvoie le nombre total
 * 
 * @param {string} [ficheGroupName] nom du groupe
 * @returns {number} Nombre de fiches dans le résultat
 */
Scrutari.SearchUnit.prototype.getFicheCount = function (ficheGroupName) {
    if (ficheGroupName) {
        for(let group of this.ficheGroupArray) {
            if ((group.category) && (group.category.name === ficheGroupName)) {
                return group.ficheCount;
            }
        }
        return 0;
    } else {
        if (this.searchMeta) {
            return this.searchMeta.ficheCount;
        } else {
            return -1;
        }
    }
};

/**
 * Retourne le type de regroupement des fiches. Peut avoir deux valeurs : « unique »
 * (toutes les fiches appartienent à un seul et unique groupe)
 * ou « category » (les fiches sont regroupées par catégorie)
 * 
 * @returns {string} Type du regroupement des fiches
 */
Scrutari.SearchUnit.prototype.getFicheGroupType = function () {
    var type = this.ficheSearchResult.ficheGroupType;
    if (type === "none") {
        type = "unique";
    }
    return type;
};


/**
 * Retourne le tableau des fiches du résultat dans le cas de l'absence de catégories
 * 
 * @returns {Scrutari~FicheGroupApiObject[]} tableau des fiches
 */
Scrutari.SearchUnit.prototype.getUniqueFicheArray = function () {
    var group = this.getFicheGroup();
    if (!group) {
        return new Array();
    }
    return group.ficheArray;
};

/**
 * Objet pagination en argument
 * 
 * @typedef Scrutari.SearchUnit~paginationObject
 * @type {Object}
 * @property {number} length longueur d'une pagination
 * @property {number} number numéro de la pagination
 */

/**
 * Objet argument de la méthode isLoaded
 * 
 * @typedef Scrutari.SearchUnit~isLoadedArgs
 * @type {Object}
 * @property {string} [ficheGroupName] nom du groupe concerné
 * @property {Scrutari.SearchUnit~paginationObject} [pagination] pagination pour laquelle tester
 */

/**
 * Indique si la plage est déjà chargée
 * 
 * @param {Scrutari.SearchUnit~isLoadedArgs} args objet argument
 * @returns {boolean}
 */
Scrutari.SearchUnit.prototype.isLoaded = function (args) {
    var ficheGroupName = args.ficheGroupName;
    var pagination = args.pagination;
    var group = this.getFicheGroup(ficheGroupName);
    if (!group) {
        return true;
    }
    if (group.ficheArray.length === group.ficheCount) {
        return true;
    }
    if (!pagination) {
        return false;
    }
    var endIndex = (pagination.length * pagination.number) - 1;
    if (endIndex < group.ficheArray.length) {
        return true;
    }
    return false;
};

/**
 * Objet argument de la méthode loadFiches
 * 
 * @typedef Scrutari.SearchUnit~loadFichesArgs
 * @type {Object}
 * @property {string} [ficheGroupName] nom du groupe concerné
 * @property {Scrutari.SearchUnit~paginationObject} [pagination] pagination pour laquelle tester
 * @property {Scrutari.SearchUnit~loadFichesCallback} [callback] fonction de retour (aucun argument transmis)
 */


/**
 * @summary charge les fiches nécessaires pour couvrir la plage demandée
 * 
 * @description
 * Si pagination n'est pas défini, charge toutes les fiches restantes
 * 
 * @param {Scrutari.SearchUnit~loadFichesArgs} args objet argument
 * @returns {void}
 */
Scrutari.SearchUnit.prototype.loadFiches = function (args) {
    var searchUnit = this;
    var ficheGroupName = args.ficheGroupName;
    var pagination = args.pagination;
    var completeFicheFunction = searchUnit.completeFicheFunction;
    var ficheCount, length, limit;
    var currentGroup = searchUnit.getFicheGroup(ficheGroupName);
    if (!currentGroup) {
        return true;
    }
    ficheCount = currentGroup.ficheCount;
    length = currentGroup.ficheArray.length;
    if (length === ficheCount) {
        return;
    }
    if (pagination) {
        limit = (pagination.length * (pagination.number + 2)) - length;
        if (limit < 1) {
            return;
        }
    } else {
        limit = -1;
    }
    var requestParameters = {
        qid: searchUnit.getQId(),
        start: length +1,
        limit: limit
    };
    if (ficheGroupName) {
        requestParameters.starttype = "in:" + ficheGroupName;
    }
    this.api.loadExistingFicheSearch({
        requestParameters: requestParameters,
        callback: function (ficheSearchResult) {
            if (ficheGroupName) {
                for(let newGroup of ficheSearchResult.ficheGroupArray) {
                    if (newGroup.category.name === currentGroup.category.name) {
                        _concat(currentGroup, newGroup);
                    }
                }
            } else {
                if (ficheSearchResult.ficheGroupArray.length > 0) {
                    _concat(currentGroup, ficheSearchResult.ficheGroupArray[0]);
                }
            }
            if (args.callback) {
                args.callback();
            }
        }
    });
    
    function _concat(currentGroup, newGroup) {
        for(let fiche of newGroup.ficheArray) {
            completeFicheFunction(searchUnit, fiche, ficheGroupName);
            currentGroup.ficheArray.push(fiche);
        }
    }
};

/**
 * Objet argument de la méthode isLoaded
 * 
 * @typedef Scrutari.SearchUnit~extractFicheArrayArgs
 * @type {Object}
 * @property {string} [ficheGroupName] nom du groupe concerné
 * @property {Scrutari.SearchUnit~paginationObject} [pagination] pagination pour laquelle tester
 */

/**
 * Retourne la liste des fiches de la plage correspondante, à utiliser
 * 
 * @param {Scrutari.SearchUnit~extractFicheArrayArgs} args objet argument
 * @returns {Scrutari~FicheApiObject[]} tableau des fiches
 */
Scrutari.SearchUnit.prototype.extractFicheArray = function (args) {
    var ficheGroupName = args.ficheGroupName;
    var pagination = args.pagination;
    let group = this.getFicheGroup(ficheGroupName);
    if (!group) {
        return new Array();
    }
    return Scrutari.Utils.extractFicheArray(group.ficheArray, pagination.length, pagination.number);
};

/**
 * @summary Renvoie le groupe de fiches pour un groupe donné.
 * 
 * @description
 * Retourne le premier groupe si ficheGroupName n'est pas défini.
 * 
 * @param {string} [ficheGroupName] Nom du groupe
 * @returns {Scrutari~FicheGroupApiObject} Groupe de fiches
 */
Scrutari.SearchUnit.prototype.getFicheGroup = function (ficheGroupName) {
    if (!ficheGroupName) {
        if (this.ficheGroupArray.length === 0) {
            return null;
        }
        return this.ficheGroupArray[0];
    }
    for(let ficheGroup of this.ficheGroupArray) {
        if ((ficheGroup.category) && (ficheGroup.category.name === ficheGroupName)) {
            return ficheGroup;
        }
    }
    return null;
};


/**
 * Retourne le mot-clé correspondant au code en argument
 * 
 * @param {number} code code du mot-clé
 * @returns {Scrutari~MotcleApiObject} Objet mot-clé de l'API correspondant au code
 */
Scrutari.SearchUnit.prototype.getMotcle = function (code) {
    var key = "code_" + code;
    if (this.motcleMap.hasOwnProperty(key)) {
        return this.motcleMap[key];
    } else {
        return null;
    }
};

Scrutari.SearchUnit.prototype.getMotcleArray = function (motcleFilter) {
    if (!motcleFilter) {
        motcleFilter = _accept;
    }
    var result = new Array();
    if (this.ficheSearchResult.hasOwnProperty("motcleArray")) {
        for(let motcle of this.ficheSearchResult.motcleArray) {
            if (motcleFilter(motcle)) {
                result.push(motcle);
            }
        }
    }
    return result;
    
    
    function _accept(motcle) {
        return true;
    }
    
};

/**
 * Fonction de retour  d'une instance de SearchUnit
 * après un appel Ajax à l'API de Scrutari
 * 
 * @callback Scrutari.SearchUnit~callback
 * @param {Scrutari.SearchUnit} searchUnit
 * @returns {void}
 */

/**
 * Fonction de retour utilisée après chargement d'une nouvelle plage de fiches (pagination), ne prend pas d'argument
 * 
 * @callback Scrutari.SearchUnit~loadFichesCallback
 * @returns {void}
  */