/* global Scrutari */

/**
 * @param {Scrutari.Client} client
 * @constructor
 */
Scrutari.History = function (client) {
    this.client = client;
    this.currentSearchUnit = null;
    this.mainCurrentSearchUnit = null;
    this._historyNumber = 0;
    this._searchUnitMap = new Map();
};

/**
 * 
 * @param {Scrutari.SearchUnit} searchUnit
 * @returns {void}
 */
Scrutari.History.prototype.addSearchUnit = function (searchUnit) {
    var history = this;
    var client = this.client;
    var searchOrigin = searchUnit.searchOrigin;
    this.currentSearchUnit = searchUnit;
    if (searchOrigin === "mainsearch") {
        history.mainCurrentSearchUnit = searchUnit;
        _addToHistory();
    } else if (searchOrigin === "subsearch") {
        let subsearchText =  "+ " + Scrutari.Utils.formatSearchSequence(client, searchUnit) + " = " + searchUnit.getFicheCount();
        if (history.currentHistoryName) {
            client.$component(client.$block(history.currentHistoryName), "subsearch").text(subsearchText);
        }
    }
    
    function _addToHistory () {
        let $historyListBlock = client.$block("historyList");
        if (!Scrutari.jQuery.exists($historyListBlock)) {
            return;
        }
        if (history.currentHistoryName) {
            var $historyBlock = client.$block(history.currentHistoryName);
            $historyBlock.removeClass(client.cssClasses.activeHistory);
            client.$component($historyBlock, "subsearch").empty();
        }
        let historyName = "history_" + history.newHistoryNumber();
        history.updateCurrent(historyName);
        history.storeSearchUnit(historyName, searchUnit);
        let contextObj = {
            searchUnit: searchUnit,
            name: historyName,
            fichestat: searchUnit.getFicheCount(),
            sequence: Scrutari.Utils.formatSearchSequence(client, searchUnit)
        };
        let html = client.render("history", contextObj);
        if (client.options.historyAtLast) {
             $historyListBlock.append(html);
        } else {
            $historyListBlock.prepend(html);
        }
    }
};

Scrutari.History.prototype.removeHistory = function (historyName) {
    this.unstoreSearchUnit(historyName);
    this.client.$block(historyName).remove();
};

/**
 * 
 * @param {string} historyName
 * @returns {void}
 */
Scrutari.History.prototype.loadHistory = function (historyName) {
    var history = this;
    var client = this.client;
    var historySearchUnit = history.getSearchUnit(historyName);
    if (historySearchUnit) {
        let $historyBlock = client.$block(historyName);
        history.currentSearchUnit = historySearchUnit;
        history.mainCurrentSearchUnit = historySearchUnit;
        history.updateCurrent(historyName);
        $historyBlock.addClass(client.cssClasses.activeHistory);
        client.displaySearchUnit(historySearchUnit, false);
        if (client.functions.searchCallbacks.mainsearch) {
            client.functions.searchCallbacks.mainsearch(client, historySearchUnit);
        }
    }
};

/**
 * 
 * @param {string} historyName
 * @returns {void}
 */
Scrutari.History.prototype.updateCurrent = function (historyName) {
     var client = this.client;
    if (this.currentHistoryName) {
        let $historyBlock = client.$block(this.currentHistoryName);
        $historyBlock.removeClass(client.cssClasses.activeHistory);
        client.$component($historyBlock, "subsearch").empty();
    }
    this.currentHistoryName = historyName; 
};

/**
 * 
 * @returns {number}
 */
Scrutari.History.prototype.newHistoryNumber = function () {
    this._historyNumber++;
    return this._historyNumber;
};


/**
 * 
 * @param {string} historyName
 * @param {Scrutari.SearchUnit} searchUnit
 * @returns {void}
 */
Scrutari.History.prototype.storeSearchUnit = function (historyName, searchUnit) {
    this._searchUnitMap.set(historyName, searchUnit);
};

/**
 * 
 * @param {string} historyName
 * @returns {void}
 */
Scrutari.History.prototype.unstoreSearchUnit = function (historyName) {
    this._searchUnitMap.delete(historyName);
};

/**
 * Retourne l'instance de Scrutari.SearchUnit stockée dans l'historique
 * avec le nom historyName
 * 
 * @param {string} historyName Nom dans l'historique
 * @returns {Scrutari.SearchUnit}
 */
Scrutari.History.prototype.getSearchUnit = function (historyName) {
    return this._searchUnitMap.get(historyName);
};

/**
 * 
 * @returns {void}
 */
Scrutari.History.prototype.clear = function () {
    this._searchUnitMap.clear();
    this.client.$block("historyList").empty();
};