/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Objet global définissant l'espace de nom Scrutari. {@link http://www.scrutari.net/|Site de Scrutari}
 * 
 * @namespace
 */
var Scrutari = {};

/**
 * Affiche le message dans la console si elle est opérationnelle.
 * 
 * @param {string} msg Message à afficher
 * @returns {void}
 */
Scrutari.log = function (msg) {
    if ((console) && (console.log)) {
        console.log(msg);
    }
};

/**
 * Affiche l'erreur dans la console si elle est opérationnelle.
 * L'argument est un objet correspondant à l'API du moteur Scrutari
 * http://www.scrutari.net/dokuwiki/serveurscrutari:json:messages 
 * 
 * @param {Object} error Objet décrivant l'erreur suivant l'API de Scrutari
 * @returns {void}
 */
Scrutari.logError = function (error) {
    var errorMessage = "Scrutari Request Error [key = " + error.key + " | parameter = " + error.parameter;
    if (error.hasOwnProperty("value")) {
        errorMessage += " | value = " + error.value;
    }
    if (error.hasOwnProperty("array")) {
        errorMessage += " | array = (";
        for(var i = 0; i < error.array.length; i++) {
            if (i > 0) {
                errorMessage += ";";
            }
            var obj = error.array[i];
            errorMessage += obj.key;
            if (obj.hasOwnProperty("value")) {
                errorMessage += "=" + obj.value;
            }
        }
        errorMessage += ")";
    }
    errorMessage += "}";
    Scrutari.log(errorMessage);
};

 /**
  * Remplace les caractères spéciaux par leurs entités HTML afin d'éviter
  * l'insertion de code HTML indésirables.
  * 
  * @param {string} text Texte à échapper
  * @returns {string} Texte échappé
  */
 Scrutari.escape = function (text) {
     var result = "";
    for(let i = 0; i < text.length; i++) {
        let carac = text.charAt(i);
        switch (carac) {
            case '&':
                result += "&amp;";
                break;
            case '"':
                result += "&quot;";
                break;
            case '<':
                result += "&lt;";
                break;
            case '>':
                result += "&gt;";
                break;
            case '\'':
                result += "&#x27;";
                break;
            default:
                result += carac;
        }
    }
    return result;
 };
 
/**
 * Vérifie la présence d'un attribut
 * 
 * @param {Scrutari~FicheApiObject} fiche à tester
 * @param {?string} attributeKey clé de l'attribut
 * @returns {boolean} présence ou non de l'attribut
 */
Scrutari.hasAttribute = function (fiche, attributeKey) {
    if (!fiche.attrMap) {
        return false;
    }
    if (attributeKey) {
        return fiche.attrMap.hasOwnProperty(attributeKey);
    } else {
        return true;
    }
};

/**
 * @summary Vérifie la présence d'un attribut marqué (propriété mattrMap).
 * @description
 * Si attributeKey est indéfini, la fonction vérifie la présence d'au moins un attribut
 * (existence de la propriété fiche.mattrMap)
 * 
 * @param {Scrutari~FicheApiObject} fiche à tester
 * @param {?string} attributeKey clé de l'attribut
 * @returns {boolean} présence ou non de l'attribut
 */
Scrutari.hasMarkedAttribute = function (fiche, attributeKey) {
    if (!fiche.mattrMap) {
        return false;
    }
    if (attributeKey) {
        return fiche.mattrMap.hasOwnProperty(attributeKey);
    } else {
        return true;
    }
};


/**
 * Fonction de tri de bases. Destiné à être utilisé dans Array.sort()
 * 
 * @typedef Scrutari~baseSort
 * @type {function}
 * @param {Scrutari~BaseApiObject} base1 Première base
 * @param {Scrutari~BaseApiObject} base2 Seconde base
 * @returns {number}  - 1 si base1 est avant base2, 1 si base1 est après base2, 0 sinon
 */

/**
 * Fonction de tri de catégories. Destiné à être utilisé dans Array.sort()
 * 
 * @typedef Scrutari~categorySort
 * @type {function}
 * @param {Scrutari~CategoryApiObject} category1 Première catégorie
 * @param {Scrutari~CategoryApiObject} category2 Seconde catégorie
 * @returns {number}  - 1 si category1 est avant category2, 1 si category1 est après category2, 0 sinon
 */

/**
 * Fonction de tri des corpus. Destiné à être utilisé dans Array.sort()
 * 
 * @typedef Scrutari~corpusSort
 * @type {function}
 * @param {Scrutari~CorpusApiObject} corpus1 Premier groupe
 * @param {Scrutari~CorpusApiObject} corpus2 Second groupe
 * @returns {number}  - 1 si corpus1 est avant corpus2, 1 si corpus1 est après corpus2, 0 sinon
 */

/**
 * Fonction de tri de groupes de résultats. Destiné à être utilisé dans Array.sort()
 * 
 * @typedef Scrutari~groupSort
 * @type {function}
 * @param {Scrutari~FicheGroupApiObject} group1 Premier groupe
 * @param {Scrutari~FicheGroupApiObject} group2 Second groupe
 * @returns {number}  - 1 si group1 est avant group2, 1 si group1 est après group2, 0 sinon
 */

/**
 * Fonction de tri de langues. Destiné à être utilisé dans Array.sort()
 * 
 * @typedef Scrutari~langSort
 * @type {function}
 * @param {Scrutari~LangStatApiObject} lang1 Première langue
 * @param {Scrutari~LangStatApiObject} lang2 Seconde langue
 * @returns {number}  - 1 si lang1 est avant lang2, 1 si lang1 est après lang2, 0 sinon
 */

/**
 * Fonction de tri des thésaurus. Destiné à être utilisé dans Array.sort()
 * 
 * @typedef Scrutari~motcleProvider
 * @type {function}
 * @param {number} code code du mot-clé
 * @returns {Scrutari~MotcleApiObject}  Objet mot-clé de l'API correspondant au code
 */

/**
 * Fonction de tri des thésaurus. Destiné à être utilisé dans Array.sort()
 * 
 * @typedef Scrutari~thesaurusSort
 * @type {function}
 * @param {Scrutari~ThesaurusApiObject} thesaurus1 Premier thésaurus
 * @param {Scrutari~ThesaurusApiObject} thesaurus2 Second thésaurus
 * @returns {number}  - 1 si group1 est avant thesaurus2, 1 si thesaurus1 est après thesaurus2, 0 sinon
 */

/**
 * Objet de définition d'un attribut renvoyé par l'API contenu dans EngineInfo.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:ype_engine type=engine}
 * 
 * @typedef Scrutari~AttributeDefApiObject
 * @type {Object}
 * @property {string} name nom complet de l'attribut
 * @property {string} ns espace de nom de l'attribut
 * @property {string} key clé locale de l'attribut
 * @property {string} type type de l'attribut (valeur : inline, list ou block)
 * @property {string} title titre de l'attribut dans la langue d'interface
 * @property {phraseMap} name  tableau associatif des intitulés associés à l'attribut
 */

/**
 * Objet base renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_base type=base}
 * 
 * @typedef Scrutari~BaseApiObject
 * @type {Object}
 */

/**
 * Objet catégorie renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_category type=category}
 * 
 * @typedef Scrutari~CategoryApiObject
 * @type {Object}
 */

/**
 * Objet corpus renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_corpus type=corpus}
 * 
 * @typedef Scrutari~CorpusApiObject
 * @type {Object}
 */

/**
 * Objet EngineInfo renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_engine type=engine}
 * 
 * @typedef Scrutari~EngineInfoApiObject
 * @type {Object}
 * @property {Object} baseMap tableau associatif des bases
 * @property {Object} corpusMap tableau associatif des corpus
 */

/**
 * Objet Error renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:messages messages}
 * 
 * @typedef Scrutari~ErrorApiObject
 * @type {Object}
 * @property {string} key clé de l'erreur
 * @property {string} parameter paramètre source de l'erreur
 * @property {string} [value] valeur de paramètre à l'origine de l'erreur
 * @property {Object[]} array tableau des erreurs détaillant l'erreur principale
 */


/**
 * Objet fiche renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_fiche}
 * 
 * @typedef Scrutari~FicheApiObject
 * @type {Object}
 */

/**
 * Objet ficheSearchResult renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_qfiche type=q-fiche}
 * 
 * @typedef Scrutari~FicheSearchResultApiObject
 * @type {Object}
 * @property {string} ficheGroupType Type du regroupement
 * @property {Scrutari~FicheGroupApiObject[]} ficheGroupArray tableau des groupes de fiches
 */

/**
 * Objet ficheGroup renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_qfiche type=q-fiche}
 * 
 * @typedef Scrutari~FicheGroupApiObject
 * @type {Object}
 * @property {Object} category objet décrivant la catégorie du groupement, présent uniquement si le moteur a des catégories
 * @property {number} ficheCount Nombre total de fiches dans le groupe
 * @property {Scrutari~FicheApiObject[]} ficheArray 
 */

/**
 * Objet contenant les informations de statistiques d'une langue.
 * C'est la popriété stats.langArray telle que définie dans 
 * {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_engine type=engine}
 * 
 * @typedef Scrutari~LangStatApiObject
 * @type {Object}
 * @property {string} lang code ISO de la langue
 * @property {number} fiche nombre total de fiches
 */

/**
 * Objet mot-clé renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_motcle}
 * 
 * @typedef Scrutari~MotcleApiObject
 * @type {Object}
 */

/**
 * Objet thésaurus renvoyé par l'API.
 * Voir la documentation de l'API {@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_thesaurus}
 * 
 * @typedef Scrutari~ThesaurusApiObject
 * @type {Object}
 */
