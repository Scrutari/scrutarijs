/* eslint-disable indent, no-nested-ternary, space-infix-ops */
/**
    @overview 
    @version 0.0.4
    @example
        
*/
const xmlWriter = new XmlWriter({prettyXml: true});

const helper = require('jsdoc/util/templateHelper');

const hasOwnProp = Object.prototype.hasOwnProperty;

function writeDescription(doclet) {
    if (doclet.summary) {
        let summaryText = helper.resolveLinks(doclet.summary);
        xmlWriter.openTag("summary");
        xmlWriter.addCData(summaryText);
        xmlWriter.closeTag("summary", false);
    }
    if (doclet.description) {
        let descriptionText = helper.resolveLinks(doclet.description);
        xmlWriter.openTag("description");
        xmlWriter.addCData(descriptionText);
        xmlWriter.closeTag("description", false);
    }
}

function writePropertyArray(doclet) {
    if (!doclet.properties) {
        return;
    }
    for(let property of doclet.properties) {
        xmlWriter.openTag("property", {
            'name': property.name
        });
        writeDescription(property);
        writeTypeArray(property);
        writeAttributes(property);
        xmlWriter.closeTag("property");
    }
}

function writeParameterArray(doclet) {
    if (!doclet.params) {
        return;
    }
    var firstLevel = new Array();
    var parentItem = null;
    for (let param of doclet.params) {
        if (!_isChild(param)) {
            let paramRegExp = null;
            try {
                 paramRegExp = new RegExp('^(?:' + param.name + '(?:\\[\\])*)\\.(.+)$');
            }   catch (e) {
                // there's probably a typo in the JSDoc comment that resulted in a weird
                // parameter name
            }
            let item = {
                param: param,
                subparams: new Array(),
                regExp: paramRegExp
            };
            firstLevel.push(item);
            parentItem = item;
        }
    }
    for(let item of firstLevel) {
        let param = item.param;
        xmlWriter.openTag("parameter", {name: param.name});
        writeDescription(param);
        writeTypeArray(param);
        for(let subparam of item.subparams) {
            xmlWriter.openTag("property", {name: subparam.name});
            writeDescription(subparam);
            writeTypeArray(subparam);
            writeAttributes(subparam);
            xmlWriter.closeTag("property");
        }
        writeAttributes(param);
        xmlWriter.closeTag("parameter");
    }
    
    
    function _isChild(param) {
        if (parentItem && parentItem.regExp) {
            let result = parentItem.regExp.exec(param.name);
            if (result) {
                param.name = result[1];
                parentItem.subparams.push(param);
                return true;
            }
        }
        return false;
    }
    
}

function writeExampleArray(doclet) {
    if (doclet.examples) {
        /*for (i = 0, len = doclet.examples.length; i < len; i++) {
            thisFunction.examples.push(doclet.examples[i]);
        }*/
    }
}

function writeReturns(doclet) {
    if (doclet.returns) {
        for(let returns of doclet.returns) {
            xmlWriter.openTag("returns");
            writeDescription(returns);
            writeTypeArray(returns);
            xmlWriter.closeTag("returns");
        }
    }
}

function writeTypeArray(typeHolder) {
    if (typeHolder.type) {
        for(let name of typeHolder.type.names) {
            let isArray;
            if (name.indexOf("Array.<") === 0) {
                isArray = true;
                name = name.substring("Array.<".length, name.length - 1);
            }
            let typeAttributes = {
                name: name,
                array: isArray
            };
            xmlWriter.addEmptyElement("type", typeAttributes);
        }
    }
}

function writeAttributes(doclet) {
    if (doclet.optional) {
        _write("optional", "true");
    }
    if (doclet.hasOwnProperty('defaultvalue')) {
        _write("default", doclet.defaultvalue);
    }
    if (doclet.nullable) {
        _write("nullable", "true");
    }
    
    
    function _write(name, value) {
        xmlWriter.addEmptyElement("attribute", {
            name: name,
            value: value
        });
    }
    
}


function graft(docletArray, parentLongname) {
    docletArray
    .filter(({memberof}) => memberof === parentLongname)
    .forEach(doclet => {
        if (doclet.kind === 'namespace') {
            let thisNamespace = {
                'name': doclet.name,
                'longname': doclet.longname,
                'access': doclet.access,
                'virtual': Boolean(doclet.virtual)
            };
            xmlWriter.openTag("namespace", thisNamespace);
            writeDescription(doclet);
            graft(docletArray, doclet.longname);
            xmlWriter.closeTag("namespace");
        }
        else if (doclet.kind === 'mixin') {
            let thisMixin = {
                'name': doclet.name,
                'longname': doclet.longname,
                'access': doclet.access,
                'virtual': Boolean(doclet.virtual)
            };
            xmlWriter.openTag("mixin", thisMixin);
            writeDescription(doclet);
            graft(docletArray, doclet.longname);
            xmlWriter.closeTag("mixin");
        }
        else if (doclet.kind === 'function') {
            let thisFunction = {
                'name': doclet.name,
                'longname': doclet.longname,
                'access': doclet.access,
                'virtual': Boolean(doclet.virtual),
                'scope': doclet.scope
            };
            xmlWriter.openTag("function", thisFunction);
            writeDescription(doclet);
            writeParameterArray(doclet);
            writeReturns(doclet);
            writeExampleArray(doclet);
            xmlWriter.closeTag("function");
        }
        else if (doclet.kind === 'member') {
            let thisMember = {
                'name': doclet.name,
                'longname': doclet.longname,
                'access': doclet.access,
                'virtual': Boolean(doclet.virtual)
            };
            xmlWriter.openTag("member", thisMember);
            writeDescription(doclet);
            writeTypeArray(doclet);
            writePropertyArray(doclet);
            xmlWriter.closeTag("member");
        }
        else if (doclet.kind === 'event') {
            let thisEvent = {
                'name': doclet.name,
                'longname': doclet.longname,
                'access': doclet.access,
                'virtual': Boolean(doclet.virtual)
            };
            xmlWriter.openTag("event", thisEvent);
            writeDescription(doclet);
            writeReturns(doclet);
            writeParameterArray(doclet);
            xmlWriter.closeTag("event");
        }
        else if (doclet.kind === 'class') {
            let thisClass = {
                'name': doclet.name,
                'longname': doclet.longname,
                'access': doclet.access,
                'virtual': Boolean(doclet.virtual),
                'fires': doclet.fires,
                'hideconstructor': doclet.hideconstructor
            };
            xmlWriter.openTag("class", thisClass);
            if (doclet.classdesc) {
                let descriptionText = helper.resolveLinks(doclet.classdesc);
                xmlWriter.openTag("description");
                xmlWriter.addCData(descriptionText);
                xmlWriter.closeTag("description", false);
            }
            if (doclet.augments) {
                for(let augment of doclet.augments) {
                    xmlWriter.addSimpleElement("extends", augment);
                }
            }
            xmlWriter.openTag("constructor");
            writeDescription(doclet);
            writeParameterArray(doclet);
            writeExampleArray(doclet);
            xmlWriter.closeTag("constructor");
            graft(docletArray, doclet.longname);
            xmlWriter.closeTag("class");
       }
       else if (doclet.kind === 'typedef') {
            let typedefAttributes = {
                'name': doclet.name,
                'longname': doclet.longname
            };
            xmlWriter.openTag("typedef", typedefAttributes);
            writeDescription(doclet);
            writeTypeArray(doclet);
            writePropertyArray(doclet);
            writeParameterArray(doclet);
            writeReturns(doclet);
            xmlWriter.closeTag("typedef");
       }
    });
}

/**
    @param {TAFFY} data
    @param {object} opts
 */
exports.publish = (data, {destination, query}) => {
    data = helper.prune(data);
    xmlWriter.appendXMLDeclaration();
    xmlWriter.appendXSLDeclaration("../../jsdoc/public/xslt/transformation.xsl");
    xmlWriter.openTag("jsdoc");
    graft(data().get()); // <-- an array of Doclet objects
    xmlWriter.closeTag("jsdoc");
    console.log(xmlWriter.xml);
};


function XmlWriter (options) {
    this.xml = "";
    this.indentLength = -999999;
    if (options) {
        if (options.indentLength) {
            this.indentLength = options.indentLength;
        } else if (options.prettyXml) {
            this.indentLength = 0;
        } 
    }
};

XmlWriter.prototype.appendXMLDeclaration = function () {
    this.write('<?xml version="1.0" encoding="UTF-8"?>');
    if (this.indentLength < 0) {
        this.write('\n');
    }
};

XmlWriter.prototype.appendXSLDeclaration = function (href) {
    this.write('\n');
    this.write('<?xml-stylesheet href="' + href + '" type="text/xsl"?>');
    if (this.indentLength < 0) {
        this.write('\n');
    }
};

XmlWriter.prototype.startOpenTag = function (tagName, indentBefore) {
    if (indentBefore === undefined) {
        indentBefore = true;
    }
    if (indentBefore) {
        this.appendIndent();
    }
    this.write('<');
    this.write(tagName);
};

XmlWriter.prototype.endOpenTag = function () {
    this.write('>');
    this.increaseIndentValue();
};

XmlWriter.prototype.closeEmptyTag = function () {
    this.write('/');
    this.write('>');
};

XmlWriter.prototype.openTag = function (tagName, attributes, indentBefore) {
    this.startOpenTag(tagName, indentBefore);
    if (attributes) {
        this.addAttributes(attributes);
    }
    this.endOpenTag();    
};

XmlWriter.prototype.closeTag = function (tagName, indentBefore) {
    if (indentBefore === undefined) {
        indentBefore = true;
    }
    this.decreaseIndentValue();
    if (indentBefore) {
        this.appendIndent();
    }
    this.write('<');
    this.write('/');
    this.write(tagName);
    this.write('>');
};

XmlWriter.prototype.addText = function (text) {
    if (text) {
        this.escape(text);
    }
};

XmlWriter.prototype.addCData = function (text) {
    this.write("<![CDATA[");
    var length = text.length;
    var carac;
    for (var i = 0; i < length; i++) {
        carac = text.charAt(i);
        if (carac === ']') {
            if ((i < (length - 1)) && (text.charAt(i + 1) === ']')) {
                this.write("]]>");
                this.write("]]");
                this.write("<![CDATA[");
                i = i + 1;
            } else {
                this.write(']');
            }
        } else {
            this.write(carac);
        }
    }
    this.write("]]>");
};

XmlWriter.prototype.addAttribute = function (attributeName, value) {
    if ((value === 0) || (value)) {
        this.write(' ');
        this.escape(attributeName);
        this.write('=');
        this.write('\"');
        this.escape(value.toString());
        this.write('\"');
    }
};

XmlWriter.prototype.addAttributes = function (object) {
    for(let prop in object) {
        this.addAttribute(prop, object[prop]);
    }
};

XmlWriter.prototype.addSimpleElement = function (tagName, value) {
    if (value) {
        this.startOpenTag(tagName);
        this.endOpenTag();
        this.addText(value);
        this.closeTag(tagName, false);
    }
};

XmlWriter.prototype.addEmptyElement = function (tagName, attributes) {
    this.startOpenTag(tagName);
    if (attributes) {
        this.addAttributes(attributes);
    }
    this.closeEmptyTag();
};

XmlWriter.prototype.write = function (text) {
    this.xml += text;
};

XmlWriter.prototype.escape = function (text) {
    var carac;
    for(var i = 0, len = text.length; i < len; i++) {
        carac = text.charAt(i);
        switch (carac) {
            case '&':
                this.write("&amp;");
                break;
            case '"':
                this.write("&quot;");
                break;
            case '<':
                this.write("&lt;");
                break;
            case '>':
                this.write("&gt;");
                break;
            case '\'':
                this.write("&apos;");
                break;
            case '\u00A0':
                this.write("&#x00A0;");
                break;
            default:
                this.write(carac);
        }
    }
};

XmlWriter.prototype.appendIndent = function () {
    if (this.indentLength > -1) {
        this.write('\n');
        for(var i = 0, len = this.indentLength; i < len; i++) {
            this.write('\t');
        }
    }
};
               
XmlWriter.prototype.increaseIndentValue = function () {
    this.indentLength = this.indentLength + 1;
};

XmlWriter.prototype.decreaseIndentValue = function () {
    this.indentLength = this.indentLength - 1;
};
