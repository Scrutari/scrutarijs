Journal des version de ScrutariJs
=================================

## 1.3.2 (2022-10-19)

Réorganisation de fichiers dans frameworks/ (attention à bien placer l'appel du fichier « framework » après l'appel de scrutarijs.js).

Traduction en portugais

Bogue de l'absence d'échappement de caractères HTML


## 1.3.1 (2021-12-15)

Documentation avec JsDoc

Prise en compte de l'introduction de la langue de l'internaute dans la pondération

## 1.3 (2020-09-11)

Points d'entrée (hook) dans le code Javascript

Décomposition en sous-gabarit du gabarit d'une fiche avec possibilité de paramètrage (option ficheBodyList)


## 1.2 (2019-12-12)

Réorganisation des fichiers spécifiques aux frameworks

Framework « none » pour fonctionner en l'absence de Framework (Bootstrap n'est plus obligatoire)

Liste des mots proches (un caractère de différence) quand une recherche ne donne pas de résultat

Réorganisation de la colonne de droite

Bouton « Relancer la recherche »


## 1.1 (2017-11-05)

Appel de la version 3 de l'API

Affichage des vignettes

Possibilité d'initialisation des filtres

Propriétés rajoutées dans une fiche commençant par _

## 1.0 (2017-09-11)

Point de départ du suivi de version de ScrutariJs
