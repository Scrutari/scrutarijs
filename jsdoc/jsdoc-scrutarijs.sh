#!/bin/bash
rm -rf ../doc/
mkdir -p ../doc/public
jsdoc -c conf-all.json ../src/js -d ../doc/all
jsdoc -c conf-public.json ../src/js > ../doc/public/jsdoc-scrutarijs.xml
xsltproc -param INCLUDE_DOCTYPE 1 -o ../doc/public/index.html public/xslt/transformation.xsl ../doc/public/jsdoc-scrutarijs.xml
cp -r public/static ../doc/public/
#rm ../doc/public/jsdoc-scrutarijs.xml
