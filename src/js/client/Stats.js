/* global Scrutari */

/**
 * @classdesc
 * Objet traitant des statistiques
 * 
 * @class
 * @param {Scrutari.Meta} scrutariMeta
 */
Scrutari.Stats = function (scrutariMeta) {
    this.unitArray = new Array();
    var maps = {
        category: {},
        base: {},
        corpus: {},
        lang: {}
    };
    this.maps = maps;
    this.filterState = null;
    for(let corpus of scrutariMeta.getCorpusArray()) {
        let langArray = corpus.stats.langArray;
        let category = scrutariMeta.getCategoryForCorpus(corpus.codecorpus);
        let categoryName = "";
        if (category) {
            categoryName = category.name;
        }
        for(let lang of langArray) {
            let unit = new Scrutari.Stats.Unit(categoryName, corpus.codebase, corpus.codecorpus, lang.lang, lang.fiche);
            this.unitArray.push(unit);
            _addInMap("category", unit, categoryName);
            _addInMap("base", unit, "code_" + corpus.codebase);
            _addInMap("corpus", unit, "code_" + corpus.codecorpus);
            _addInMap("lang", unit, lang.lang);
            
        }
    }
    
    function _addInMap(type, unit, key) {
        var map = maps[type];
        if (!map.hasOwnProperty(key)) {
            map[key] = new Array();
        }
        map[key].push(unit);
    }
};

Scrutari.Stats.prototype.update = function (filterState) {
    this.filterState = filterState;
};

Scrutari.Stats.prototype.getFicheCount = function (type, key) {
    key = Scrutari.Utils.checkKey(type, key);
    var map = this.maps[type];
    if (!map.hasOwnProperty(key)) {
        return 0;
    }
    var count = 0;
    for(let unit of map[key]) {
        count += unit.check(this.filterState);
    }
    return count;
};

Scrutari.Stats.Unit = function (category, base, corpus, lang, value) {
    this.category = category;
    this.base = base;
    this.corpus = corpus;
    this.lang = lang;
    this.value = value;
};

Scrutari.Stats.Unit.prototype.check = function (filterState) {
    if (!filterState) {
        return this.value;
    }
    if (!filterState.contains("category", this.category)) {
        return 0;
    }
    if (!filterState.contains("base", this.base)) {
        return 0;
    }
    if (!filterState.contains("corpus", this.corpus)) {
        return 0;
    }
    if (!filterState.contains("lang", this.lang)) {
        return 0;
    }
    return this.value;
};
