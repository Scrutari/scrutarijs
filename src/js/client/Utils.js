/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Contient des fonctions utilitaires « statiques ». Aucune de ces fonctions ne doit altérer
 * l'état de leurs arguments ou de variables globales.
 * 
 * @namespace
 * @private
 */
Scrutari.Utils = {};

/**
 * Répartit les objets dans objectArray dans l'ensemble des élements HTML définis
 * par jqArgument.
 * objectTemplate est la fonction à utiliser pour produire le code html de l'objet.
 * Elle doit prendre comme argument un objet de objectArray
 */
Scrutari.Utils.divideIntoColumns = function (objectArray, jqArgument, objectTemplate) {
    var objectCount = objectArray.length;
    if (objectCount === 0) {
        return;
    }
    var $elements = Scrutari.jQuery.convert(jqArgument);
    var elementCount = $elements.length;
    if (elementCount === 0) {
        Scrutari.log("HtmlElement selection with jqArgument is empty ");
        return;
    }
    var objectCount = objectArray.length;
    if (objectCount <= elementCount) {
        for(let i = 0; i < objectCount; i++) {
            $($elements[i]).append(objectTemplate(objectArray[i]));
        }
        return;
    }
    var modulo = objectCount % elementCount;
    var columnLength = (objectCount - modulo) / elementCount;
    var start = 0;
    var stop = 0;
    for(let i = 0; i< elementCount; i++) {
        let $element = $($elements[i]);
        stop += columnLength;
        if (i < modulo) {
            stop++;
        }
        for(let j = start; j < stop; j++) {
            $element.append(objectTemplate(objectArray[j]));
        }
        start = stop;
    }
};

Scrutari.Utils.getTabArray = function (ficheCount, paginationLength, currentPaginationNumber) {
    var result = new Array();
    var paginationCount;
    if (ficheCount <= paginationLength) {
        paginationCount = 1;
        return result;
    } else {
        let modulo = ficheCount % paginationLength;
        paginationCount = (ficheCount - modulo) / paginationLength;
        if (modulo > 0) {
            paginationCount ++;
        }
    }
    if (currentPaginationNumber > paginationCount) {
        currentPaginationNumber = paginationCount;
    }
    var paginationNumberStart = 1;
    var paginationNumberEnd = 9;
    if (currentPaginationNumber > 6) {
       
        paginationNumberStart = currentPaginationNumber - 3;
        paginationNumberEnd = currentPaginationNumber + 3;
    }
    if (paginationNumberEnd > paginationCount) {
        paginationNumberEnd = paginationCount;
    }
    if (paginationNumberStart > 1) {
        result.push({
                number: 1,
                title: "1",
                state: 'enabled'
        });
        result.push({
                number: 0,
                title: "…",
                state: 'etc'
        });
    }
    for(let i = paginationNumberStart; i <= paginationNumberEnd; i++) {
        let state = 'enabled';
        if (i === currentPaginationNumber) {
            state = 'active';
        }
        result.push({
                number: i,
                title: i.toString(),
                state: state
        });
    }
    if (paginationNumberEnd < paginationCount) {
        result.push({
                number: 0,
                title: "…",
                state: 'etc'
        });
    }
    return result;
};

/**
 * Désactive les éléments de selectors
 */
Scrutari.Utils.disable = function (jqArgument) {
    var $elements = Scrutari.jQuery.convert(jqArgument);
    $elements.prop('disabled', true);
    return $elements;
};

/**
 * Active les éléments de selectors
 */
Scrutari.Utils.enable = function (jqArgument) {
    var $elements = Scrutari.jQuery.convert(jqArgument);
    $elements.prop('disabled', false);
    return $elements;
};

/**
 * Coche les éléments de selectors
 */
Scrutari.Utils.uncheck = function (jqArgument) {
    var $elements = Scrutari.jQuery.convert(jqArgument);
    $elements.prop('checked', false);
    return $elements;
};

/**
 * Décoche les éléments de selectors
 */
Scrutari.Utils.check = function (jqArgument) {
    var $elements = Scrutari.jQuery.convert(jqArgument);
    $elements.prop('checked', true);
    return $elements;
};

/**
 * Bascule de la valeur « on » à « off » ou « off » à « on » pour la clé stateDataKey
 */
Scrutari.Utils.toggle = function (jqElement, stateDataKey) {
    var state = jqElement.data(stateDataKey);
    if (state === 'off') {
        state = 'on';
    } else {
        state = 'off';
    }
    jqElement.data(stateDataKey, state);
    return state;
};

/**
 * Active ou désactive les éléments au sens HTML (attribut disabled)
 * en fonction de la valeur de state (« off » ou « on »)
 */
Scrutari.Utils.toggle.disabled = function (jqArgument, state) {
    var $elements = Scrutari.jQuery.convert(jqArgument);
    if (state === 'off') {
        $elements.prop('disabled', true);
    } else {
        $elements.prop('disabled', false);
    }
    return $elements;
};

/**
 * Change le texte actuel par celui de la clé alterDataKey et conserve
 * l'ancienne valeur du texte dans le clé alterDataKey
 */
Scrutari.Utils.toggle.text = function (jqArgument, alterDataKey) {
    var $elements = Scrutari.jQuery.convert(jqArgument);
    var length = $elements.length;
    for(let i = 0; i < length; i++) {
        let jqEl = $($elements[i]);
        let currentText =jqEl.text();
        let alterText = jqEl.data(alterDataKey);
        jqEl.text(alterText);
        jqEl.data(alterDataKey, currentText);
    }
    return $elements;
};

/**
 * Bascule de la classe onClass à offClass si state est égal à « off »
 * ou sinon de la classe offClass à la classe onClass
 */
Scrutari.Utils.toggle.classes = function (jqArgument, state, onClass, offClass) {
    var $elements = Scrutari.jQuery.convert(jqArgument);
    if (state === 'off') {
        $elements.addClass(offClass).removeClass(onClass);
    } else {
        $elements.removeClass(offClass).addClass(onClass);
    }
    return $elements;
};

Scrutari.Utils.getBaseSortFunction = function (baseSortType, locales) {
    var compareFunction = Scrutari.Utils.getCompareLocaleFunction(locales);
    switch(baseSortType) {
        case "fiche-count":
            return _ficheCountSort;
        case "title":
            return _titleSort;
        default:
            return null;
    }
    
    
    function _ficheCountSort(base1, base2) {
        var count1 = base1.stats.fiche;
        var count2 = base2.stats.fiche;
        if (count1 > count2) {
            return -1;
        } else if (count1 < count2) {
            return 1;
        } else {
            return Scrutari.Utils.compareCodebase(base1, base2);
        }
    }
    
    function _titleSort(base1, base2) {
        var title1 = base1.title;
        var title2 = base2.title;
        if (!title1) {
            title1 = "";
        }
        if (!title2) {
            title2 = "";
        }
        var comp = compareFunction(title1, title2);
        if (comp !== 0) {
            return comp;
        } else {
            return Scrutari.Utils.compareCodebase(base1, base2);
        }
    }
    
};

Scrutari.Utils.getCorpusSortFunction = function (corpusSortType, locales) {
    var _ficheCountSort = function (corpus1, corpus2) {
        var count1 = corpus1.stats.fiche;
        var count2 = corpus2.stats.fiche;
        if (count1 > count2) {
            return -1;
        } else if (count1 < count2) {
            return 1;
        } else {
            return Scrutari.Utils.compareCodecorpus(corpus1, corpus2);
        }
    };
    var compareFunction = Scrutari.Utils.getCompareLocaleFunction(locales);
    var _titleSort = function (corpus1, corpus2) {
        var title1 = corpus1.title;
        var title2 = corpus2.title;
        if (!title1) {
            title1 = "";
        }
        if (!title2) {
            title2 = "";
        }
        var comp = compareFunction(title1, title2);
        if (comp !== 0) {
            return comp;
        } else {
            return Scrutari.Utils.compareCodecorpus(corpus1, corpus2);
        }
    };
    switch(corpusSortType) {
        case "fiche-count":
            return _ficheCountSort;
        case "title":
            return _titleSort;
        default:
            return null;
    }
};

Scrutari.Utils.getCategorySortFunction = function (categorySortType, locales) {
    var compareFunction = Scrutari.Utils.getCompareLocaleFunction(locales);
    switch(categorySortType) {
        case "rank":
            return _rankSort;
        case "fiche-count":
            return _ficheCountSort;
        case "title":
            return _titleSort;
        default:
            return null;
    }
    
    function _rankSort(category1, category2) {
        let count1 = category1.rank;
        let count2 = category2.rank;
        if (count1 > count2) {
            return -1;
        } else if (count1 < count2) {
            return 1;
        } else {
            let code1 = category1.name;
            let code2 = category2.name;
            if (code1 < code2) {
                return -1;
            } else if (code1 > code2) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    
    function _ficheCountSort(category1, category2) {
        let count1 = category1.stats.fiche;
        let count2 = category2.stats.fiche;
        if (count1 > count2) {
            return -1;
        } else if (count1 < count2) {
            return 1;
        } else {
            return _rankSort(category1, category2);
        }
    }
    
    function _titleSort(category1, category2) {
        let title1 = category1.title;
        let title2 = category2.title;
        if (!title1) {
            title1 = "";
        }
        if (!title2) {
            title2 = "";
        }
        let comp = compareFunction(title1, title2);
        if (comp !== 0) {
            return comp;
        } else {
            return _rankSort(category1, category2);
        }
    }
    
};

Scrutari.Utils.getLangSortFunction = function (langSortType, locales) {
    var compareFunction = Scrutari.Utils.getCompareLocaleFunction(locales);
    switch(langSortType) {
        case "code":
            return _codeSort;
        case "fiche-count":
            return _ficheCountSort;
        case "title":
            return _titleSort;
        default:
            return null;
    }
    
    function _codeSort(lang1, lang2) {
        let code1 = lang1.lang;
        let code2 = lang2.lang;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
    
    function _ficheCountSort(lang1, lang2) {
        let count1 = lang1.fiche;
        let count2 = lang2.fiche;
        if (count1 > count2) {
            return -1;
        } else if (count1 < count2) {
            return 1;
        } else {
            return _codeSort(lang1, lang2);
        }
    }
    
    function _titleSort(lang1, lang2) {
        let title1 = lang1.title;
        let title2 = lang2.title;
        if (!title1) {
            title1 = lang1.lang;
        }
        if (!title2) {
            title2 = lang2.lang;
        }
        let comp = compareFunction(title1, title2);
        if (comp !== 0) {
            return comp;
        } else {
            return _codeSort(lang1, lang2);
        }
    }
    
};

Scrutari.Utils.getGroupSortFunction = function (groupSortType) {
    switch(groupSortType) {
        case "fiche-count":
            return _ficheCountSort;
        default:
            return null;
    }
    
    function _ficheCountSort(group1, group2) {
        let count1 = group1.ficheCount;
        let count2 = group2.ficheCount;
        if (count1 > count2) {
            return -1;
        } else if (count1 < count2) {
            return 1;
        } else {
            let rank1 = group1.category.rank;
            let rank2 = group1.category.rank;
            if (rank1 < rank2) {
                return -1;
            } else if (rank1 > rank2) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    
};

 /**
  * Tableau d'un élément « marqué » (mtitre, mvalue)
  * 
  * @returns {string} Code HTML correspondant
  */
Scrutari.Utils.mark = function (markArray, classAttribute) {
    if (!classAttribute) {
        classAttribute = "scrutari-Mark";
    }
    var html = "";
    for (let obj of markArray) {
        if (typeof obj === 'string') {
            html += Scrutari.escape(obj);
        } else if (obj.s) {
            html += "<span class='" + classAttribute + "'>";
            html += Scrutari.escape(obj.s);
            html += "</span>";
        }
    }
    return html;
};

 /**
  * Transforme un tableau marqué en chaine
  * 
  * @returns {string} Chaine nettoyée du marquage
  */
Scrutari.Utils.unmark = function (markArray) {
    var text = "";
    for (let obj of markArray) {
        if (typeof obj === 'string') {
            text += obj;
        } else if (obj.s) {
            text += obj.s;
        }
    }
    return text;
};

Scrutari.Utils.formatSearchSequence = function (client, searchUnit) {
    var q = searchUnit.getQ();
    q = q.replace(/\&\&/g, client.loc('_ and'));
    q = q.replace(/\|\|/g, client.loc('_ or'));
    return q;
};

Scrutari.Utils.render = function (templateString, object) {
    var result = templateString;
    for(let key in object) {
        let normalReplace = new RegExp("{{:" + key + "}}", 'g');
        let escapeReplace = new RegExp("{{>" + key + "}}", 'g');
        let value = object[key];
        result = result.replace(normalReplace, value);
        result = result.replace(escapeReplace, Scrutari.escape(value));
    }
    return result;
};

Scrutari.Utils.compareCodebase = function (obj1, obj2) {
    var code1 = obj1.codebase;
    var code2 = obj2.codebase;
    if (code1 < code2) {
        return -1;
    } else if (code1 > code2) {
        return 1;
    } else {
        return 0;
    }
};

Scrutari.Utils.compareCodecorpus = function (obj1, obj2) {
    var code1 = obj1.codecorpus;
    var code2 = obj2.codecorpus;
    if (code1 < code2) {
        return -1;
    } else if (code1 > code2) {
        return 1;
    } else {
        return 0;
    }
};

Scrutari.Utils.getCompareLocaleFunction = function (locales) {
    var _localeCompareSupportsLocales = function () {
        try {
            "a".localeCompare("b", "i");
        } catch (exception) {
            return exception.name === "RangeError";
        }
        return false;
    };
    var _localesCompare = function (string1, string2) {
        return string1.localeCompare(string2, locales, {
            usage: "sort",
            sensitivity: "base",
            ignorePunctuation: true});
    };
    var _oldCompare = function (string1, string2) {
        string1 = string1.toLowerCase();
        string2 = string2.toLowerCase();
        return string1.localeCompare(string2);
    };
    if (_localeCompareSupportsLocales()) {
        return _localesCompare;
    } else {
        return _oldCompare;
    }
};

Scrutari.Utils.buildCorpusMap = function (scrutariMeta, arrays) {
    var corpusMap = new Object();
    var finalCount = 0;
    if (arrays.categoryArray)  {
        finalCount++;
        let arrayForCategories = scrutariMeta.getCorpusArrayForCategories(arrays.categoryArray);
        for(let i = 0; i < arrayForCategories.length; i++) {
            let key = "code_" + arrayForCategories[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    if (arrays.baseArray)  {
        finalCount++;
        let arrayForBases = scrutariMeta.getCorpusArrayForBases(arrays.baseArray);
        for(let i = 0; i < arrayForBases.length; i++) {
            let key = "code_" + arrayForBases[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    if (arrays.corpusArrays)  {
        finalCount++;
        for(let i = 0; i < arrays.corpusArrays.length; i++) {
            let key = "code_" + arrays.corpusArrays[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    corpusMap.completeValue = finalCount;
    return corpusMap;
};

Scrutari.Utils.checkKey = function (type, value) {
    switch(type) {
        case "base":
        case "corpus":
            return "code_" + value;
    }
    return value;
};

Scrutari.Utils.hasFilter = function (requestParameters) {
    for(let prop in requestParameters) {
        switch(prop) {
            case "baselist":
            case "corpuslist":
            case "categorylist":
            case "langlist":
                var value = requestParameters[prop];
                if (value) {
                    return true;
                }
        }
        if (prop.indexOf("flt") === 0) {
            return true;
        }
    }
    return false;
};

Scrutari.Utils.extractFicheArray = function (ficheArray, paginationLength, paginationNumber) {
    var startIndex = paginationLength * (paginationNumber - 1);
    var length = ficheArray.length;
    var extractArray = new Array();
    if (startIndex >= length) {
        return extractArray;
    }
    var min = Math.min(ficheArray.length, startIndex + paginationLength);
    for(let i = startIndex; i < min; i++) {
        extractArray.push(ficheArray[i]);
    }
    return extractArray;
};
