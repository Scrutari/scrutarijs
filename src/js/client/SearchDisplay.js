/* global Scrutari */

/**
 * @param {Scrutari.Client} client
 * @param {Scrutari.SearchUnit} searchUnit
 * @constructor
 */
Scrutari.SearchDisplay = function (client, searchUnit) {
    this.client = client;
    this.searchUnit = searchUnit;
};

Scrutari.SearchDisplay.prototype.run = function () {
    var client = this.client;
    var searchUnit = this.searchUnit;
    client.hook("beforeSearchDisplay", searchUnit);
    client.clearPagination();
    var ficheCount = searchUnit.getFicheCount();
    var $paginationBlock = this.getPaginationBlock();
    if (!this.checkNotEmpty(ficheCount, $paginationBlock)) {
        return;
    }
    this.checkThreshold(ficheCount);
    this.updateLinks();
    this.addPaginations($paginationBlock);
    client.hook("afterSearchDisplay", searchUnit);
};

Scrutari.SearchDisplay.prototype.getPaginationBlock = function () {
    var $paginationBlock = this.client.$block("paginationBlock");
    $paginationBlock.empty();
    return $paginationBlock;
};

Scrutari.SearchDisplay.prototype.checkNotEmpty = function (ficheCount, $paginationBlock) {
    var client = this.client;
    client.$hidden("start", 'show');
    if (ficheCount === 0) {
        this.processEmpty($paginationBlock);
        return false;
    } else {
        this.client.$hidden("empty", 'show');
        return true;
    }
};

Scrutari.SearchDisplay.prototype.processEmpty = function ($paginationBlock) {
    var client = this.client;
    var searchUnit = this.searchUnit;
    client.$hidden("empty", 'hide');
    let withFilter;
    if (this.searchOrigin === "subsearch") {
        withFilter = true;
    } else {
        withFilter = Scrutari.Utils.hasFilter(searchUnit.requestParameters);
    }
    $paginationBlock.html(client.render("pagination_empty", {withFilter: withFilter, searchUnit: searchUnit}));
};

Scrutari.SearchDisplay.prototype.checkThreshold = function (ficheCount) {
    var client = this.client;
    if (ficheCount >= client.api.options.subsearchThreshold) {
        client.$hidden("threshold", 'show');
    } else {
        client.$hidden("threshold", 'hide');
    }
};

Scrutari.SearchDisplay.prototype.updateLinks = function () {
    var client = this.client;
    var qId = this.searchUnit.getQId();
    var permalink = client.toPermalink(qId);
    if (permalink) {
        client.$link("permalink").attr("href", permalink);
        client.$label("permalinkValue").html(permalink);
    }
    _updateDownloadHref("ods");
    _updateDownloadHref("csv");
    _updateDownloadHref("atom");
    
    function _updateDownloadHref(extension) {
        client.$link(extension).attr("href", client.api.getDownloadUrl(qId, extension));
    }
};

Scrutari.SearchDisplay.prototype.addPaginations = function ($paginationBlock) {
    if (this.searchUnit.getFicheGroupType() === 'category') {
        this.addCategoryPaginations($paginationBlock);
    } else {
        this.addUniquePagination($paginationBlock);
    }
};

Scrutari.SearchDisplay.prototype.addCategoryPaginations = function ($paginationBlock) {
    var client = this.client;
    var searchUnit = this.searchUnit;
    var contextObj = {
        searchUnit: searchUnit,
        array: new Array()
    };
    let active = true;
    for(let ficheGroup of searchUnit.ficheGroupArray) {
        let category = ficheGroup.category;
        let metaCategory = client.scrutariMeta.getCategory(category.name);
        if (metaCategory) {
            category = metaCategory;
        } else {
            category.phraseMap = {};
            category.attrMap = {};
        }
        let description = "";
        if (category.phraseMap.description) {
            description = category.phraseMap.description;
        }
        contextObj.array.push({
            category: category,
            name: category.name,
            title: category.title,
            description: description,
            active: active,
            fichestat: ficheGroup.ficheCount
        });
        active = false;
    }
    $paginationBlock.html(client.render("pagination_groups", contextObj));
    for(let group of contextObj.array) {
        let ficheGroupName = group.name;
        let pagination = new Scrutari.Pagination(client, searchUnit, ficheGroupName);
        client.putPagination(ficheGroupName, pagination);
        pagination.change(1);
    }
};

Scrutari.SearchDisplay.prototype.addUniquePagination = function ($paginationBlock) {
    var client = this.client;
    var searchUnit = this.searchUnit;
    $paginationBlock.html(client.render("pagination_unique", {searchUnit: searchUnit}));
    let uniquePagination = new Scrutari.Pagination(client, searchUnit);
    client.putPagination("", uniquePagination);
    uniquePagination.change(1);
};
