/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * @classdesc
 * Objet encapsulant un objet engineInfo obtenu via l'API Json (requête type=engine) et proposant
 * des accesseurs et des méthodes de traitement.
 * Voir http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_engine
 * 
 * @class
 * @param {Object} engineInfo Objet engineInfo de l'API Scrutari
 * @hideconstructor
 */
Scrutari.Meta = function (engineInfo) {
    this.engineInfo = engineInfo;
};

/**
 * Retourne le tableau des attributs existants pour une famille donnée.
 * Les noms de famille sont : primary, secondary et technical
 * 
 * @param {string} familyName Nom de la famille d'attributs
 * @returns {Scrutari~AttributeDefApiObject[]} Tableau des définitions
 */
Scrutari.Meta.prototype.getAttributeArray = function (familyName) {
    if (!this.engineInfo.hasOwnProperty("attributes")) {
        return new Array();
    }
    if (!this.engineInfo.attributes.hasOwnProperty(familyName)) {
        return new Array();
    }
    return this.engineInfo.attributes[familyName];
};

/**
 * Retourne la base correspondant au code en argument
 * 
 * @param {number} code Code de la base recherchée
 * @returns {Scrutari~BaseApiObject} Objet base
 */
Scrutari.Meta.prototype.getBase = function (code) {
    var key = "code_" + code;
    if (this.engineInfo.baseMap.hasOwnProperty(key)) {
        return this.engineInfo.baseMap[key];
    } else {
        return null;
    }
};

/**
 * Retourne le tableau des bases
 * 
 * @param {Scrutari~baseSort} [sortFunction] règle de tri des bases
 * @returns {Scrutari~BaseApiObject[]} Tableau des bases
 */
Scrutari.Meta.prototype.getBaseArray = function (sortFunction) {
    var array = new Array();
    var baseMap = this.engineInfo.baseMap;
    var p=0;
    for(let prop in baseMap) {
        array[p] = baseMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};

/**
 * Retourne la catégorie correspondant au nom en argument
 * 
 * @param {string} categoryName Nom de la catégorie recherchée
 * @returns {Scrutari~CategoryApiObject} Objet catégorie
 */
Scrutari.Meta.prototype.getCategory = function (categoryName) {
    if (this.engineInfo.hasOwnProperty("categoryMap")) {
        if (this.engineInfo.categoryMap.hasOwnProperty(categoryName)) {
            return this.engineInfo.categoryMap[categoryName];
        } else {
            return null;
        }
    } else {
        return null;
    }
};
 
/**
 * Retourne le tableau des catégories
 * 
 * @param {Scrutari~categorySort} [sortFunction] règle de tri des catégories
 * @returns {Scrutari~CategoryApiObject[]} Tableau des catégories
 */
Scrutari.Meta.prototype.getCategoryArray = function (sortFunction) {
    var array = new Array();
    if (!this.engineInfo.hasOwnProperty("categoryMap")) {
        return array;
    }
    var categoryMap = this.engineInfo.categoryMap;
    var p=0;
    for(let prop in categoryMap) {
        array[p] = categoryMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};

 /**
 * Retourne la catégorie correspondant au corpus dont le code est donnée en argument
 * 
 * @param {number} code code du corpus
 * @returns {Scrutari~CategoryApiObject} Objet catégorie
 */
Scrutari.Meta.prototype.getCategoryForCorpus = function (code) {
     if (!this.engineInfo.hasOwnProperty("categoryMap")) {
        return null;
    }
    var categoryMap = this.engineInfo.categoryMap;
    for(let prop in categoryMap) {
        let category = categoryMap[prop];
        for(let codecorpus of category.codecorpusArray) {
            if (codecorpus === code) {
                return category;
            }
        }
    }
    return null;
 };

/**
 * Retourne le libellé du complément de numéro complementNumber pour le corpus
 * de cod
 * 
 * @param {number} code code du corpus
 * @param {number} complementNumber Numéro du complément
 * @returns {string} Libellé du complément
 */
Scrutari.Meta.prototype.getComplementTitle = function(code, complementNumber) {
    var corpus = this.getCorpus(code);
    if (!corpus) {
        return "";
    }
    var key = "complement_" + complementNumber;
    if (corpus.phraseMap.hasOwnProperty(key)) {
        return corpus.phraseMap[key];
    } else {
        return key;
    }
};

/**
 * Retourne le corpus correspondant au code en argument
 * 
 * @param {number} code Code du corpus recherché
 * @returns {Scrutari~CorpusApiObject} Objet corpus
 */
Scrutari.Meta.prototype.getCorpus = function (code) {
    var key = "code_" + code;
    if (this.engineInfo.corpusMap.hasOwnProperty(key)) {
        return this.engineInfo.corpusMap[key];
    } else {
        return null;
    }
};

/**
 * Retourne le tableau des corpus
 * 
 * @param {Scrutari~corpusSort} [sortFunction] règle de tri des corpus
 * @returns {Scrutari~CorpusApiObject[]} Tableau des corpus
 */
Scrutari.Meta.prototype.getCorpusArray = function (sortFunction) {
    var array = new Array();
    var corpusMap = this.engineInfo.corpusMap;
    var p=0;
    for(let prop in corpusMap) {
        array[p] = corpusMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};

/**
 * Retourne le tableau des codes corpus pour les bases
 * dont les codes sont en argument
 * 
 * @param {number[]} baseArray Codes des bases
 * @returns {number[]} Tableau des codes des corpus
 */
Scrutari.Meta.prototype.getCorpusArrayForBases = function (baseArray) {
    var result = new Array();
    for(let codebase of baseArray) {
        let key = "code_" +codebase;
        if (this.engineInfo.baseMap.hasOwnProperty(key)) {
            result = result.concat(this.engineInfo.baseMap[key].codecorpusArray);
        }
    }
    return result;
};

/**
 * Retourne un tableau avec le code des corpus correspondant aux catégories
 * categoryArray étant un tableau des noms de catégorie
 * 
 * @param {string[]} categoryArray Noms des catégories
 * @returns {number[]} Tableau des codes des corpus
 */
Scrutari.Meta.prototype.getCorpusArrayForCategories = function (categoryArray) {
    var result = new Array();
    if (!this.engineInfo.hasOwnProperty("categoryMap")) {
        return result;
    }
    for(let  categoryName of categoryArray) {
        if (this.engineInfo.categoryMap.hasOwnProperty(categoryName)) {
            result = result.concat(this.engineInfo.categoryMap[categoryName].codecorpusArray);
        }
    }
    return result;
};

/**
 * Retourne le nombre total de fiches pour le corpus dont le code est donné
 * en argument
 * 
 * @param {number} code
 * @returns {number} Nombre total de fiches du corpus
 */
Scrutari.Meta.prototype.getCorpusFicheCount = function (code) {
    var corpus = this.getCorpus(code);
    if (!corpus) {
        return 0;
    }
    return corpus.stats.fiche;
};

/**
 * Retourne le nombre total de fiches pour le corpus dont le code est donné
 * en argument pour les langues indiquées par le tableau langArray
 * 
 * @param {number} code
 * @param {string[]} langArray tableau des langues
 * @returns {number} Nombre total de fiches du corpus
 */
Scrutari.Meta.prototype.getCorpusLangFicheCount = function (code, langArray) {
    var corpus = this.getCorpus(code);
    if (!corpus) {
        return 0;
    }
    var ficheCount = 0;
    for(let langObj of corpus.stats.langArray) {
        if (langArray.indexOf(langObj.lang) !== -1) {
            ficheCount += langObj.fiche;
        }
    }
    return ficheCount;
};

/**
 * Retourne les options définies par le moteur
 * 
 * @returns {Object} options Options définies dans le moteur
 */
Scrutari.Meta.prototype.getDefaultOptions = function () {
    var options = {};
    var attrMap = this.engineInfo.metadata.attrMap;
    for(let key in attrMap) {
        if (key.indexOf("scrutarijs:") === 0) {
            let name = key.substring("scrutarijs:".length);
            let values = attrMap[key];
            if (values.length === 1) {
                let value = values[0];
                if (value === "false") {
                    value = false;
                } else if (value === "true") {
                    value = true;
                }
                options[name] = value;
            } else {
                options[name] = values;
            }
        }
        this[key] = options[key];
    }
    return options;
};

/**
 * Retourne l'objet EngineInfo encapsulé.
 * 
 * @returns {Scrutari~EngineInfoApiObject} engineInfo Information sur le moteur
 */
Scrutari.Meta.prototype.getEngineInfo = function () {
    return this.engineInfo;
};

/**
 * Retourne le nombre total (toutes bases confondues) de fiches
 * recensées par le moteur Scrutari
 * 
 * @returns {number} Nombre total de fiches
 */
Scrutari.Meta.prototype.getGlobalFicheCount = function () {
    return this.engineInfo.stats.fiche;
};

/**
 * Retourne le nombre total (toutes bases confondues) de fiches
 * recensées par le moteur Scrutari pour les langues indiquées par le
 * tableau langArray
 *  * 
 * @param {string[]} langArray tableau des langues
 * @returns {number} Nombre total de fiches pour les langues données
 */
Scrutari.Meta.prototype.getGlobalLangFicheCount = function (langArray) {
    var ficheCount = 0;
    for(let langObj of this.engineInfo.stats.langArray) {
        if (langArray.indexOf(langObj.lang) !== -1) {
            ficheCount += langObj.fiche;
        }
    }
    return ficheCount;
};

/**
 * Retourne le tableau de statistiques par langues
 * 
 * @param {Scrutari~langSort} [sortFunction] règle de tri des langues
 * @returns {Scrutari~LangStatApiObject[]} Tableau des langues
 */
Scrutari.Meta.prototype.getLangArray = function (sortFunction) {
    var array = new Array();
    var length = this.engineInfo.stats.langArray.length;
    for(let i = 0; i < length; i++) {
        array[i] = this.engineInfo.stats.langArray[i];
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};

/**
 * Retourne le libellé de la langue correspondant au code ISO en argument
 * 
 * @param {type} iso code ISO de la langue
 * @returns {string} Libellé de la langue
 */
Scrutari.Meta.prototype.getLangLabel = function (iso) {
    if (this.engineInfo.langMap.hasOwnProperty(iso)) {
        return this.engineInfo.langMap[iso];
    } else {
        return iso;
    }
};

/**
 * Retourne le thésaurus correspondant au code en argument
 * 
 * @param {number} code Code du thésaurus recherché
 * @returns {Scrutari~ThesaurusApiObject} Objet thésaurus
 */
Scrutari.Meta.prototype.getThesaurus = function (code) {
    var key = "code_" + code;
    if (this.engineInfo.thesaurusMap.hasOwnProperty(key)) {
        return this.engineInfo.thesaurusMap[key];
    } else {
        return null;
    }
};

/**
 * Retourne le tableau des thésaurus
 * 
 * @param {Scrutari~thesaurusSort} [sortFunction] règle de tri des thésaurus
 * @returns {Scrutari~ThesaurusApiObject[]} Tableau des thésaurus
 */
Scrutari.Meta.prototype.getThesaurusArray = function (sortFunction) {
    var array = new Array();
    var thesaurusMap = this.engineInfo.thesaurusMap;
    var p=0;
    for(let prop in thesaurusMap) {
        array[p] = thesaurusMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};

/**
 * Retourne le libéllé du moteur
 * 
 * @returns {string} title Libellé du moteur
 */
Scrutari.Meta.prototype.getTitle = function () {
    return this.engineInfo.metadata.title;
};
 
/**
 * Indique si le moteur Scrutari propose des catégories dans sa configuration.
 * 
 * @returns {boolean} Présence de catégories
 */
Scrutari.Meta.prototype.withCategory = function () {
    return this.engineInfo.hasOwnProperty("categoryMap");
};


/**
 * Uniquement pour des raisons de rétrocompatibilité
 * 
 * @param {Scrutari.Api} api Accès au moteur Scrutari
 * @param {Scrutari.Meta~callback} callback Fonction de retour du chargement de Scrutari.Meta
 * @return {void}
 * @private 
 */
Scrutari.Meta.load = function(api, callback) {
    api.initMeta(callback);
};


/**
 * Fonction de récupération d'une instance de Scrutari.Meta
 * après un appel Ajax à l'API de Scrutari
 * 
 * @callback Scrutari.Meta~callback
 * @param {Scrutari.Meta} scrutariMeta
 * @returns {void}
 */