/* global Scrutari */

Scrutari.DefaultUi.modalAction = function (client, name, action) {
    client.$modal(name).modal(action);
};