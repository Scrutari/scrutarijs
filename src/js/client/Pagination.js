/* global Scrutari */

/**
 * @param {Scrutari.Client} client
 * @param {Scrutari.SearchUnit|Scrutari~FicheApiObject[]} source - Instance de SearchUnit ou tableau de fiches
 * @param {string} [ficheGroupName] - Nom du groupe de fiches
 * @constructor
 */
Scrutari.Pagination = function (client, source, ficheGroupName) {
    this.client = client;
    this.ficheGroupName = ficheGroupName;
    this.paginationLength = client.api.options.paginationLength;
    if (!source) {
        throw new Error("name undefined");
    }
    if (source instanceof Scrutari.SearchUnit) {
        this.searchUnit = source;
        this.customFicheArray = null;
    } else {
        this.searchUnit = null;
        this.customFicheArray = source;
    }
};

Scrutari.Pagination.prototype.change = function (paginationNumber) {
    var pagination = this;
    var client = pagination.client;
    var ficheGroupName = pagination.ficheGroupName;
    var paginationLength = pagination.paginationLength;
    var ficheCount, paginationFicheArray;
    if (pagination.searchUnit) {
        _fromSearchUnit();
    } else {
        _fromCustom();
    }
    var hookParams = _getHookParams();
    if (!paginationFicheArray) {
        return;
    }
    client.hook("beforePaginationChange", paginationFicheArray, hookParams);
    client.$block("ficheList", ficheGroupName).html(client.render("fiche", paginationFicheArray));
    _setTabs();
    client.hook("afterPaginationChange", paginationFicheArray, hookParams);
    
    function _fromSearchUnit() {
        let searchUnit = pagination.searchUnit;
        ficheCount = searchUnit.getFicheCount(ficheGroupName);
        paginationFicheArray = pagination.extractPaginationFicheArray(searchUnit, paginationNumber);
    }
    
    function _fromCustom() {
        ficheCount = pagination.customFicheArray.length;
        paginationFicheArray = Scrutari.Utils.extractFicheArray(pagination.customFicheArray, paginationLength, paginationNumber);
    }
    
    function _getHookParams() {
        if (ficheGroupName) {
            return {
                    paginationNumber: paginationNumber,
                    ficheGroupName: ficheGroupName
            };
        } else {
            return {
                paginationNumber:paginationNumber
            };
        }
    }
    
    function _setTabs() {
        var tabArray = Scrutari.Utils.getTabArray(ficheCount, paginationLength, paginationNumber);
        var topBlock = "topTabs" ;
        var bottomBlock = "bottomTabs";
        var type = "unique";
        if (ficheGroupName) {
            topBlock =  topBlock + "-" + ficheGroupName;
            bottomBlock = bottomBlock + "-" + ficheGroupName;
            type = "group";
        }
        var template = client.getTemplate("tabs");
        client.$block(topBlock).html(template({
           tabArray: tabArray,
           type: type,
           ficheGroupName: ficheGroupName,
           position: "top"
        }));
        client.$block(bottomBlock).html(template({
           tabArray: tabArray,
           type: type,
           ficheGroupName: ficheGroupName,
           position: "bottom"
        }));
    }

};

Scrutari.Pagination.prototype.extractPaginationFicheArray = function (searchUnit, paginationNumber) {
    var pagination = this;
    var client = this.client;
    var paginationArg = {
            length: pagination.paginationLength,
            number: paginationNumber
    };
    var ficheGroupName = this.ficheGroupName;
    if (!searchUnit.isLoaded({pagination: paginationArg, ficheGroupName: ficheGroupName})) {
        if (client.isWaiting) {
            return;
        }
        client.$block("ficheList", ficheGroupName).html(client.render("loading", {searchUnit: searchUnit}));
        client.isWaiting = true;
        searchUnit.loadFiches({
            pagination: paginationArg,
            ficheGroupName: ficheGroupName,
            callback:function () {
                client.isWaiting = false;
                pagination.change(paginationNumber);
            }
        });
        return false;
    }
    return searchUnit.extractFicheArray({pagination: paginationArg, ficheGroupName: ficheGroupName});
};
