<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="function[@scope='instance']" mode="fn_Signature">
        <span class="syntax-ReturnType">
            <xsl:apply-templates select="returns/type" mode="fn_Signature"/>
        </span>
        <xsl:text> </xsl:text>
        <span class="syntax-Function">
            <xsl:value-of select="@name"/>
        </span>
    </xsl:template>
    
    <xsl:template match="function[@scope='static']" mode="fn_Signature">
        <span class="syntax-ReturnType">
            <xsl:apply-templates select="returns/type" mode="fn_Signature"/>
        </span>
        <xsl:text> </xsl:text>
        <span class="syntax-Function">
            <xsl:value-of select="../@longname"/>.<xsl:value-of select="@name"/>
        </span>
    </xsl:template>
    
    <xsl:template match="constructor"  mode="fn_Signature">
        <span class="syntax-Type_new">new</span>
        <xsl:text> </xsl:text>
        <span class="syntax-Function">
            <xsl:value-of select="../@longname"/>
        </span>
    </xsl:template>
    
    <xsl:template match="typedef" mode="fn_Signature">
        <xsl:if test="returns/type[@name != 'void']">
            <span class="syntax-ReturnType">
                <xsl:apply-templates select="returns/type" mode="fn_Signature"/>
            </span>
        </xsl:if>
        <xsl:text> </xsl:text>
        <span class="syntax-Type_function">function</span>
    </xsl:template>
    
    <xsl:template match="parameter" mode="fn_Signature">
        <xsl:if test="position() != 1"><span class="syntax-Operator">,</span><xsl:text> </xsl:text></xsl:if>
        <xsl:apply-templates select="type" mode="fn_Signature"/>
        <xsl:text> </xsl:text>
        <span class="syntax-Param"><xsl:value-of select="@name"/></span>
    </xsl:template>
    
    <xsl:template match="parameter[attribute[@name='optional']]" mode="fn_Signature">
        <xsl:choose>
        <xsl:when test="position() != 1">
            <span class="syntax-Operator">
                <xsl:text> [, </xsl:text>
            </span>
        </xsl:when>
        <xsl:otherwise>
            <span class="syntax-Operator">[</span>
        </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="type" mode="fn_Signature"/>
        <xsl:text> </xsl:text>
        <span class="syntax-Param"><xsl:value-of select="@name"/></span>
        <span class="syntax-Operator">]</span>
    </xsl:template>
    
    <xsl:template match="type" mode="fn_Signature">
        <xsl:variable name="name" select="@name"/>
        <xsl:if test="position() &gt; 1">
            <span class="syntax-Operator">|</span>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="//class[@longname=$name]">
                <span class="syntax-Type_module">
                    <xsl:value-of select="$name"/><xsl:apply-templates select="." mode="fn_Array"/>
                </span>
            </xsl:when>
            <xsl:when test="//typedef[@longname=$name][type[@name='function']]">
                <span class="syntax-Type_function">function<xsl:apply-templates select="." mode="fn_Array"/></span>
            </xsl:when>
            <xsl:when test="//typedef[@longname=$name][type[@name='Object']]">
                <span class="syntax-Type_apiobject"><xsl:value-of select="//typedef[@longname=$name][type[@name='Object']]/@name"/><xsl:apply-templates select="." mode="fn_Array"/></span>
            </xsl:when>
            <xsl:when test="$name = 'JQuery'">
                <span class="syntax-Type_jquery">$<xsl:apply-templates select="." mode="fn_Array"/></span>
            </xsl:when>
            <xsl:otherwise>
                <span>
                    <xsl:attribute name="class"><xsl:apply-templates select="." mode="fn_Class"/></xsl:attribute>
                    <xsl:value-of select="$name"/><xsl:apply-templates select="." mode="fn_Array"/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="type" mode="fn_Array">
        <xsl:if test="@array = 'true'">[]</xsl:if>
    </xsl:template>
    
    <xsl:template match="type" mode="fn_Class">
        <xsl:variable name="name" select="@name"/>
        <xsl:choose>
            <xsl:when test="$name = 'void'">syntax-Type_void</xsl:when>
            <xsl:when test="$name = 'number'">syntax-Type_number</xsl:when>
            <xsl:when test="$name = 'boolean'">syntax-Type_boolean</xsl:when>
            <xsl:when test="$name = 'string'">syntax-Type_string</xsl:when>
            <xsl:when test="$name = 'function'">syntax-Type_function</xsl:when>
            <xsl:when test="$name = 'JQuery'">syntax-Type_jquery</xsl:when>
            <xsl:otherwise>syntax-Type_standard</xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
