/* global Scrutari */

Scrutari.DefaultUi = {};


/*****************
 * 
 * Fonctions d'initialisation principale
 */

Scrutari.DefaultUi.init = function (client) {
    var scrutariMeta = client.scrutariMeta;
    $("#" + client.clientId).html(client.compileStructure("main", client.options.structureOptions));
    client.initForms();
    client.initActions();
    client.initChangeListeners();
    Scrutari.DefaultUi.initMainTitle(client);
    client.$count('stats-global', 'update', scrutariMeta.getGlobalFicheCount());
    client.$hidden('init', 'show');
    client.show(client.$action('parametersDisplay'));
    var locales = client.api.lang;
    var langArray = scrutariMeta.getLangArray();
    if ((langArray.length > 1) && (Scrutari.jQuery.exists(client.$panel('lang')))) {
        for(let i = 0, len = langArray.length; i < len; i++) {
            let title = "";
            let code = langArray[i].lang;
            let label = scrutariMeta.getLangLabel(code);
            if (label !== code) {
                title = label;
            }
            langArray[i].title = title;
        }
        let langSortFunction = Scrutari.Utils.getLangSortFunction(client.options.langSort, locales);
        if (langSortFunction) {
            langArray = langArray.sort(langSortFunction);
        }
        Scrutari.DefaultUi.initColumns(client, langArray,"lang");
    }
    if ((scrutariMeta.withCategory()) && (Scrutari.jQuery.exists(client.$panel('category')))) {
        let categoryArray = scrutariMeta.getCategoryArray(Scrutari.Utils.getCategorySortFunction(client.options.categorySort, locales));
        Scrutari.DefaultUi.initColumns(client, categoryArray, "category");
    }
    if (client.options.withCorpus) {
        if (Scrutari.jQuery.exists(client.$panel('corpus'))) {
            let corpusArray =  scrutariMeta.getCorpusArray(Scrutari.Utils.getCorpusSortFunction(client.options.corpusSort, locales));
            if (corpusArray.length > 1) {
                Scrutari.DefaultUi.initColumns(client, corpusArray, "corpus");
            }
        }
    } else {
        if (Scrutari.jQuery.exists(client.$panel('base'))) {
            let baseArray =  scrutariMeta.getBaseArray(Scrutari.Utils.getBaseSortFunction(client.options.baseSort, locales));
            if (baseArray.length > 1) {
                 Scrutari.DefaultUi.initColumns(client, baseArray, "base");
            }
        }
    }
    var initialFilters = client.options.initialFilters;
    if (initialFilters) {
        _initFilter("baselist", "base");
        _initFilter("corpuslist","corpus");
        _initFilter("categorylist", "category");
        _initFilter("langlist", "lang");
    }
    client.checkInitialQuery(client);
    
    
    function _initFilter (optionName, target) {
        if (!initialFilters.hasOwnProperty(optionName)) {
            return;
        }
        Scrutari.DefaultUi.initFilter(client, target, initialFilters[optionName].split(","));
    }
    
};


/*****************
 * 
 * Autres fonctions par defaut de client.functions.
 */

Scrutari.DefaultUi.initFilterByQuery = function (client, searchUnit) {
    var searchOptions = searchUnit.searchMeta.options;
    if (searchOptions) {
        _initFilterByQuery(searchOptions.baselist, "base");
        _initFilterByQuery(searchOptions.corpuslist, "corpus");
        _initFilterByQuery(searchOptions.categorylist, "category");
        _initFilterByQuery(searchOptions.langlist, "lang");
    }
    
    
    function _initFilterByQuery (list, target) {
        if (list) {
            Scrutari.DefaultUi.initFilter(client, target, list.array);
        }
    }
};

Scrutari.DefaultUi.initForms = function (client) {
    client.$form('mainsearch').submit(function () {
        let q = _getQ(this);
        if (q.length > 0) {
            let requestParameters = client.buildSearchRequestParameters({
                "log": "all",
                "q": q
            });
            client.startLoading();
            client.hook("newSearch", requestParameters, "mainsearch");
            client.api.loadFicheSearch({
                requestParameters: requestParameters,
                callback: function (ficheSearchResult) {
                    let searchUnit = new Scrutari.SearchUnit({
                        api: client.api,
                        ficheSearchResult: ficheSearchResult,
                        requestParameters: requestParameters,
                        completeFicheFunction: Scrutari.DefaultUi.getCompleteFicheFunction(client),
                        groupSortFunction: client.api.options.groupSortFunction,
                        searchOrigin: "mainsearch"
                    });
                    client.searchCallback(searchUnit);
                },
                errorCallback: function (error) {
                    client.errorCallback(error, "mainsearch");
                }
            });
        }
        return false;
    });
    client.$form('subsearch').submit(function () {
        let q = _getQ(this);
        if ((q.length > 0) && (client.history.mainCurrentSearchUnit)) {
            let requestParameters = client.buildSearchRequestParameters({
                "q": q,
                "flt-qid": client.history.mainCurrentSearchUnit.getQId()
            });
            client.startLoading();
            client.hook("newSearch", requestParameters, "subsearch");
            client.api.loadFicheSearch({
                requestParameters: requestParameters,
                callback: function (ficheSearchResult) {
                    let searchUnit = new Scrutari.SearchUnit({
                        api: client.api,
                        ficheSearchResult: ficheSearchResult,
                        requestParameters: requestParameters,
                        completeFicheFunction: Scrutari.DefaultUi.getCompleteFicheFunction(client),
                        groupSortFunction: client.api.options.groupSortFunction,
                        searchOrigin: "subsearch"
                    });
                    client.searchCallback(searchUnit);
                },
                errorCallback: function (error) {
                    client.errorCallback(error, "subsearch");
                }
            });
        }
        return false;
    });
    
    function _getQ(form) {
        let qInput = form["q"];
        if (!qInput) {
            Scrutari.log("missing q input");
            return "";
        }
        let q = qInput.value;
        if (q) {
            return q.trim();
        } else {
            return "";
        }
    };
};

Scrutari.DefaultUi.checkInitialQuery = function (client) {
    var initialQuery = client.options.initialQuery;
    var initialQId = client.options.initialQId;
    if (initialQuery) {
        var $mainSearchForm = client.$form('mainsearch');
        if (Scrutari.jQuery.exists($mainSearchForm)) {
            $mainSearchForm.find("input[name='q']").val(initialQuery);
            $mainSearchForm.submit();
        }
    } else if (initialQId) {
         var requestParameters = client.buildSearchRequestParameters({
             "qid": initialQId
         });
        client.startLoading();
        client.hook("newSearch", requestParameters, "qidsearch");
        client.api.loadFicheSearch({
            requestParameters: requestParameters,
            callback: function (ficheSearchResult) {
                let searchUnit = new Scrutari.SearchUnit({
                    api: client.api,
                    ficheSearchResult: ficheSearchResult,
                    requestParameters: requestParameters,
                    completeFicheFunction: Scrutari.DefaultUi.getCompleteFicheFunction(client),
                    groupSortFunction: client.api.options.groupSortFunction,
                    searchOrigin: "qidsearch"
                });
                let $mainSearchForm = client.$form('mainsearch');
                $mainSearchForm.find("input[name='q']").val(searchUnit.searchMeta.q);
                $mainSearchForm.find("input[name='q-mode'][value='operation']").click();
                if (client.functions.initFilterByQuery) {
                    client.functions.initFilterByQuery(client, searchUnit);
                }
                client.searchCallback(searchUnit);
            },
            errorCallback: function (error) {
                client.errorCallback(error, "qidsearch");
            }
        });
    }
};

Scrutari.DefaultUi.getCompleteFicheFunction = function (client) {
    return function (searchUnit, fiche, categoryName) {
        client.completeFiche(searchUnit, fiche, categoryName);
    };
};

Scrutari.DefaultUi.ignoreElement = function (client, name) {
    switch(name) {
        case "panel-base":
            return (client.options.withCorpus);
        case "panel-corpus":
            return (!client.options.withCorpus);
        case "result-share":
            return (!client.options.permalinkPattern);
    }
    return false;
};

Scrutari.DefaultUi.isFilterEnable = function (client, target) {
    return (client.$action("enablePanel", target).data("scrutariState") === "on");
};

Scrutari.DefaultUi.mainsearchCallback = function (client, searchUnit) {
    client.$count("stats-result", 'update', searchUnit.getFicheCount());
    var $parametersDisplayButton = client.$action('parametersDisplay');
    if ($parametersDisplayButton.data("scrutariState") === "on") {
        $parametersDisplayButton.click();
    }
};

Scrutari.DefaultUi.qidsearchCallback = function (client, searchUnit) {
    Scrutari.DefaultUi.mainsearchCallback(client, searchUnit);
};

Scrutari.DefaultUi.subsearchCallback = function (client, searchUnit) {
    client.$count("stats-result", 'update', searchUnit.getFicheCount());
};

/**
 * 
 * @param {Scrutari.Client} client
 * @param {string} error
 * @param {string} searchOrigin
 * @returns {void}
 */
Scrutari.DefaultUi.displayError = function (client, error, searchOrigin) {
    if (error.parameter !== "q") {
        Scrutari.logError(error);
        return;
    }
    var title =  client.loc(error.key);
    if (title !== error.key) {
        var alertMessage = title;
        if (error.hasOwnProperty("array"))  {
            alertMessage += client.loc('_ colon');
            for(var i = 0; i < error.array.length; i++) {
                alertMessage += "\n";
                var obj = error.array[i];
                alertMessage += "- ";
                alertMessage += client.loc(obj.key);
                if (obj.hasOwnProperty("value")) {
                    alertMessage += client.loc('_ colon');
                    alertMessage += " ";
                    alertMessage += obj.value;
                }
            }
        }
        alert(alertMessage);
    } else {
        Scrutari.logError(error);
    }
};

Scrutari.DefaultUi.modalAction = function (client, name, action) {
    var $modal = client.$modal(name);
    if ($modal.length === 0) {
        Scrutari.log("Unknown modal: " + name);
        return;
    }
    switch(action) {
        case 'show':
            if (!$modal.data("overlayId")) {
                var overlayId = client.startOverlay({
                    header: $modal.children("header").html(),
                    content: $modal.children("div").html(),
                    footer: $modal.children("footer").html(),
                    closeTooltip: client.loc("_ button_close"),
                    afterEnd: function () {
                        $modal.data("overlayId", null);
                    }
                });
                $modal.data("overlayId", overlayId);
            }
            break;
        case 'hide':
            client.endOverlay($modal.data("overlayId"));
            break;
    }
};

Scrutari.DefaultUi.startLoading = function (client) {
    Scrutari.DefaultUi.modalAction(client, 'loading', 'show');
};

Scrutari.DefaultUi.endLoading = function (client) {
    Scrutari.DefaultUi.modalAction(client, 'loading', 'hide');
};

Scrutari.DefaultUi.newSearchDisplay = function (client, searchUnit) {
    return new Scrutari.SearchDisplay(client, searchUnit);
};

Scrutari.DefaultUi.actionHandler = function (client, button, action, target) {
    var $button = $(button);
    switch(action) {
        case 'enablePanel':
            _enablePanel();
            break;
        case 'checkAll':
            _checkAll();
            break;
        case 'uncheckAll':
            _uncheckAll();
            break;
        case 'parametersDisplay':
            let state = Scrutari.Utils.toggle($button, "scrutariState");
            Scrutari.Utils.toggle.classes($button, state, client.cssClasses.on, "");
            if (state === 'on') {
                client.$area('parameters', 'show');
            } else {
                client.$area('parameters', 'hide');
            }
            break;
        default:
            return false;
    }
    return true;
     
    function _checkAll () {
        Scrutari.Utils.check("input[name='" + target + "']");
        Scrutari.DefaultUi.filterChange(client);
    }
    
    function _uncheckAll () {
        Scrutari.Utils.uncheck("input[name='" + target + "']");
        Scrutari.DefaultUi.filterChange(client);
    }
    
    function _enablePanel () {
        var state = Scrutari.Utils.toggle($button, "scrutariState");
        _enableChekButtons();
        Scrutari.Utils.toggle.classes($button, state, client.cssClasses.on, "");
        Scrutari.Utils.toggle.classes(client.$block("columnsBlock", target), state, "", client.cssClasses.disabled);
        Scrutari.Utils.toggle.text($button.children("span"), "scrutariAlternate");
        var $filterLabel = client.$label(target + "Filter");
        Scrutari.Utils.toggle.text($filterLabel, "scrutariAlternate");
        Scrutari.Utils.toggle.classes($filterLabel, state, client.cssClasses.activePanel, client.cssClasses.disabled);
        Scrutari.DefaultUi.filterChange(client);
        
        function _enableChekButtons () {
            let disabled = false;
            if (state === 'off') {
                disabled = true;
            }
            client.$action("checkAll", target).prop('disabled', disabled);
            client.$action("uncheckAll", target).prop('disabled', disabled);
        }
        
    }
    
};

Scrutari.DefaultUi.changeHandler = function (client, element, name) {
    switch(name) {
        case 'lang':
        case 'category':
        case 'base':
        case 'corpus':
            _checkClick(name);
            return true;
        default:
            return false;
    }

    function _checkClick (target) {
        var $button = client.$action("enablePanel", target);
        if ($button.data('scrutariState') === 'off') {
            $button.click();
            $(this).focus();
        } else {
            Scrutari.DefaultUi.filterChange(client);
        }
    }
    
};


/*****************
 * 
 * Fonctions d'initialisation
 */

Scrutari.DefaultUi.initMainTitle = function (client) {
    var html = "";
    var mainTitle = client.options.mainTitle;
    if ((mainTitle) || (mainTitle === "")) {
        if (typeof mainTitle === "function") {
            html = mainTitle(client);
        } else {
            html = mainTitle;
        }
    } else {
        html += client.loc('_ title_main');
        html += " – ";
        var title = client.scrutariMeta.getTitle();
        if (title) {
            html += Scrutari.escape(title);
        } else {
            html += "[";
            html += client.api.name;
            html += "]";
        }
    }
    client.$label('mainTitle').html(html);
};

Scrutari.DefaultUi.initFilter = function (client, target, checkedArray) {
    var done = false;
    for(var i = 0, len = checkedArray.length; i < len; i++) {
        var $input = client.$input(target, checkedArray[i]);
        if ($input.length > 0) {
            $input.prop("checked", true);
            done = true;
        }
    }
    if (done) {
        client.$action("enablePanel", target).click();
    }  
};

Scrutari.DefaultUi.initColumns = function (client, array, name) {
    Scrutari.Utils.divideIntoColumns(array, client.$block("columnsBlock", name).children("div"), client.getTemplate(name));
    client.$panel(name, 'show');
};

Scrutari.DefaultUi.filterChange = function (client) {
    var globalFicheCount = client.scrutariMeta.getGlobalFicheCount();
    var filterState = client.buildFilterState();
    var filterFicheCount;
    if (filterState.empty) {
        filterFicheCount = client.scrutariMeta.getGlobalFicheCount();
    } else {
        filterFicheCount = -1;
    }
    client.stats.update(filterState);
    if (!_checkFilterFicheCount("base")) {
        if (!_checkFilterFicheCount("corpus")) {
            if (!_checkFilterFicheCount("category")) {
                _checkFilterFicheCount("lang");
            }
        }
    }
    _updateState("base");
    _updateState("corpus");
    _updateState("category");
    _updateState("lang");
    var filterTitlesArray = new Array();
    _addFilterTitles("base", "_ label_base_one", "_ label_base_many");
    _addFilterTitles("corpus", "_ label_corpus_one", "_ label_corpus_many");
    _addFilterTitles("lang", "_ label_lang_one", "_ label_lang_many");
    _addFilterTitles("category", "_ label_category_one", "_ label_category_many");
    var $filterFicheCount = client.$count('stats-filter', 'update', filterFicheCount);
    var $filterValue = client.$component($filterFicheCount, "value");
    let filterClass = client.cssClasses.filterStat;
    let noneClass = client.cssClasses.noneStat;
    if (filterFicheCount === globalFicheCount) {
        $filterValue.removeClass(filterClass).removeClass(noneClass);
        client.$hidden('filter', 'hide');
    } else if (filterFicheCount === 0) {
        $filterValue.removeClass(filterClass).addClass(noneClass);
        client.$hidden('filter', 'show');
    } else {
        $filterValue.addClass(filterClass).removeClass(noneClass);
        client.$hidden('filter', 'show');
    }
    var $filterTitles = client.$component($filterFicheCount, "titles");
    if (filterTitlesArray.length > 0) {
        $filterTitles.html(filterTitlesArray.join(" | "));
    } else {
        $filterTitles.html("");
    }
    
    
    function _checkFilterFicheCount(type) {
        var $stat = client.$({scrutariStatType: type});
        if ($stat.length > 0) {
            filterFicheCount = 0;
            $stat.each(function (index, element) {
                let key = element.dataset.scrutariStatKey;
                filterFicheCount += client.stats.getFicheCount(type, key);
            });
            return true;
        } else {
            return false;
        }
    }
    
    
    function _updateState(type) {
        client.$({scrutariStatType: type}).each(function (index, element) {
            var $el = $(element);
            var key = element.dataset.scrutariStatKey;
            var ficheCount = client.stats.getFicheCount(type, key);
            var $statTitle = $el.parents(Scrutari.jQuery.toCssSelector({scrutariComponent: "stat-text"}));
            let excludedClass = client.cssClasses.excluded;
            if (ficheCount != element.dataset.scrutariStatDefault) {
                if (ficheCount === 0) {
                    $el.html("");
                    $statTitle.addClass(excludedClass);
                } else {
                    $statTitle.removeClass(excludedClass);
                    $el.html(client.formatNumber(ficheCount) + " / ");
                }
            } else {
                $el.html("");
                $statTitle.removeClass(excludedClass);
            }
        });
    }
    
    
    function _addFilterTitles(type, oneLocKey, manyLocKey) {
        var array = filterState.titles[type];
        if (array.length > 0) {
            var locKey = (array.length === 1)?oneLocKey:manyLocKey;
            filterTitlesArray.push(client.loc(locKey) + client.loc('_ colon') + " " + array.join(", "));
        }
    }
    
};

Scrutari.DefaultUi.buildSearchRequestParameters = function (client, customParameters) {
    var requestParameters = new Object();
    _checkFilter("baselist", "base");
    _checkFilter("corpuslist", "corpus");
    _checkFilter("categorylist", "category");
    _checkFilter("langlist", "lang");
    var qMode = _getVal(client.$input_checked("q-mode"));
    if (qMode) {
        requestParameters["q-mode"] = qMode;
    }
    var ponderation = _getVal(client.$input_checked("ponderation"));
    if (ponderation === 'date') {
        requestParameters.ponderation = '10,80,5,5';
    }
    var periode = _getVal(client.$input("periode"));
    if (periode) {
        requestParameters["flt-date"] = periode;
    }
    if (Scrutari.jQuery.exists(client.$input_checked("wildchar", "end"))) {
        requestParameters.wildchar = "end";
    } else {
        requestParameters.wildchar = "none";
    }
    if (customParameters) {
        for(let propKey in customParameters) {
            requestParameters[propKey] = customParameters[propKey];
        }
    }
    return requestParameters;
    
    
    function _getVal($el) {
        let val = $el.val();
        if (val) {
            return val.trim();
        } else {
            return "";
        }
    }
    
    function _checkFilter (paramName, target) {
        if ((client.functions.isFilterEnable) && (client.functions.isFilterEnable(client, target))) {
            var $inputs = client.$input_checked(target);
            var value = "";
            for(let i = 0; i < $inputs.length; i++) {
                if (i > 0) {
                    value += ",";
                }
                value += $inputs[i].value;
            }
            if (value.length > 0) {
                requestParameters[paramName] = value;
            }
        }
    }
};

Scrutari.DefaultUi.buildFilterState = function (client) {
    var filterState = new Scrutari.FilterState();
    _check("base");
    _check("corpus");
    _check("category");
    _check("lang");
    return filterState;
    
    
    function _check (target) {
        if (!((client.functions.isFilterEnable) && (client.functions.isFilterEnable(client, target)))) {
            return;
        }
        client.$input_checked(target).each(function (index,element) {
            filterState.add(target, element.value, element.dataset.scrutariTitle);
        });
    }
};