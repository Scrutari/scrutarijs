/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Objet stockant les options de la configuration. N'importe quelle propriété peut
 * être ajoutée aux propriétés définies par défaut en fonction des besoins.
 * 
 * @typedef Scrutari.Api~options
 * @type {Object}
 * @property {string} dataType - Type de la requête json (défaut: json)
 * @property {string} queryVariant - Variante des champs utilisée, les variantes disponibles dépendent du moteur Scrutari (défaut: query)
 * @property {string} ficheFields - Champs des fiches récupérés auprès du moteur, écrase la valeur de queryVariant si non nul (défaut: null)
 * @property {string} motcleFields - Champs des mots-clés récupérés auprès du moteur, écrase la valeur de queryVariant si non nul (défaut: null)
 * @property {number} paginationLength - nombre de résultats par plage (défaut : 50)
 * @property {number} subsearchThreshold - nombre-plancher de résultats à partir duquel une sous-recherche est proposée (défaut : 250)
 * @property {Scrutari~groupSort} groupSortFunction - fonction de classement des groupes de résultats (par défaut, les groupes sont classés par le nombre de résultats)
 */

/**
 * Objet argument du constructeur. Toute propriété de l'objet qui n'est pas dans la liste est traitée comme un option.
 * 
 * Autrement dit, une option peut être définie aussi bien comme une propriété directe de l'objet settings que comme propriété
 * de l'objet settings.options.
 * 
 * Si une propriété est présente dans les deux, c'est la propriété directe de settings qui l'emporte.
 * 
 * @typedef Scrutari.Api~settings
 * @type {Object}
 * @property {string} name - Nom de la configuration
 * @property {string} engineUrl - Adresse du moteur Scrutari
 * @property {string} lang - Langue de l'interface
 * @property {string} origin - Paramètre origin transmis au moteur Scrutari
 * @property {Scrutari.Api~options} options - Options de configuration
 * */

/**
 * @classdesc
 * Une instance de Scrutari.Api comprend les informations de configuration
 * nécessaires à l'accès au serveur Scrutari et propose des méthodes pour y accéder.
 * C'est le premier objet à construire avant appel des autres éléments de la
 * bibliothèque.
 * 
 * Le constructeur possède deux signatures : une avec un seul objet settings,
 *  l'autre avec quatre ou cinq paramètres (name, engineUrl, lang, origin, [options]).
 * 
 * @class
 * @param {Scrutari.Api~settings} settings Object décrivant la configuration
 * 
 * @property {string} name - Nom de la configuration
 * @property {string} engineUrl - Adresse du moteur Scrutari
 * @property {string} lang - Langue de l'interface
 * @property {string} origin - Paramètre origin transmis au moteur Scrutari
 * @property {Scrutari.Meta} meta - Objet meta conservé après invocation de initMeta
 * @property {Scrutari.Api~options} options - Options de l'API
 * 
 */
Scrutari.Api = function (settings) {
    var name, engineUrl, lang, origin;
    var argumentLength = arguments.length;
    var options = {
        dataType: "json",
        queryVariant: "query",
        ficheFields: null,
        motcleFields: null,
        paginationLength: 50,
        subsearchThreshold: 250,
        groupSortFunction: _ficheCountSort,
        limit: 0
    };
    if (argumentLength === 1) {
        name = settings.name;
        engineUrl = settings.engineUrl;
        lang = settings.lang;
        origin = settings.origin;
        if (settings.options) {
            _assignOptions(settings.options);
        };
        _assignOptions(settings);
    } else if ((argumentLength === 4) || (argumentLength === 5)) {
        name = arguments[0];
        engineUrl = arguments[1];
        lang = arguments[2];
        origin = arguments[3];
        if (argumentLength === 5) {
            _assignOptions(arguments[4]);
        }
    } else {
        throw new Error("Argument length should be 1, 4 or 5");
    }
    if (!name) {
        throw new Error("name undefined");
    }
    if (!engineUrl) {
        throw new Error("engineUrl undefined");
    }
    if (!lang) {
        throw new Error("lang undefined");
    }
    if (!origin) {
        throw new Error("origin undefined");
    }
    this.name = name;
    this.engineUrl = engineUrl;
    this.lang = lang;
    this.origin = origin;
    this.options = options;
    this.meta = null;
    
    function _assignOptions(object) {
        for(let key in object) {
            let value = object[key];
            switch(key) {
                case "name":
                case "engineUrl":
                case "lang":
                case "origin":
                case "options":
                    break;
                default:
                    options[key] = value;
            }
        }
    }
    
    function _ficheCountSort(group1, group2) {
        let count1 = group1.ficheCount;
        let count2 = group2.ficheCount;
        if (count1 > count2) {
            return -1;
        } else if (count1 < count2) {
            return 1;
        } else {
            let rank1 = group1.category.rank;
            let rank2 = group1.category.rank;
            if (rank1 < rank2) {
                return -1;
            } else if (rank1 > rank2) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    
};

/*
 * Rétrocompatibilité
 */
Scrutari.Config = Scrutari.Api;

/**
 * Retourne l'URL d'appel Json du moteur Scrutari
 * 
 * @returns {string} URL d'appel du Json
 */
Scrutari.Api.prototype.getJsonUrl = function () {
    return this.engineUrl + "json";
};

/**
 * Retourne l'URL de récupération du fichier correspondant à la recherche
 * possédant l'identifiant qId. Cette URL est construite à partir de l'URL du moteur
 * 
 * @param {string} qId Idenfiant de la recherche
 * @param {string} extension Extension à utiliser    
 * @returns {string} URL des données de la recherche au format de l'extension
 */
Scrutari.Api.prototype.getDownloadUrl = function (qId, extension) {
    switch(extension) {
        case "ods":
        case "csv":
            return this.engineUrl + "export/" +  "result_" + qId + "_" + this.lang + "." + extension;
        case "atom":
            return this.engineUrl + "feed/" + "fiches_" + this.lang + ".atom?qid=" + qId + "&all=" + _getCurrentDate();
        default:
            Scrutari.log("Unknown extension: " + extension);
            return "";
    }
    
    
    function _getCurrentDate() {
        var date = new Date();
        var dateString = date.getFullYear() + "-";
        var mois = date.getMonth() + 1;
        if (mois < 10) {
            dateString += "0";
        }
        dateString += mois;
        dateString += "-";
        var jour = date.getDate();
        if (jour < 10) {
            dateString += "0";
        }
        dateString += jour;
        return dateString;
    }
    
};

/**
 * Construit un permalien de fonction de permalinkPattern
 * $QUI et $LANG sont remplacés par les bonnes valeurs
 * 
 * @param {string} qId Idenfiant de la recherche
 * @param {string} permalinkPattern Gabarit de l'URL       
 * @returns {string} URL du permalien
 */
Scrutari.Api.prototype.getPermalinkUrl = function (qId, permalinkPattern) {
    var permalink = permalinkPattern.replace("$LANG", this.lang);
    permalink = permalink.replace("$QID", qId);
    return permalink;
};

/**
 * Retourne le nombre limite de résultats par invocation de la recherche.
 * Construit par options.limit ou sinon sur options.paginationLength
 * 
 * @returns {number} nombre limite de résultats
 */
Scrutari.Api.prototype.getLimit = function () {
    if (this.options.limit) {
        return this.options.limit;
    } else {
        return this.options.paginationLength * 2;
    }
};

/**
 * Construit les paramètres d'une requête Ajax.
 * En plus du premier argument type, cette méthode peut avoir un nombre illimité d'objets
 * comme argument. Les propriétés de ces objets seront attibués à l'objet résultat, avec priorité
 * pour le dernier objet.
 * 
 * Exemple d'appel : buildRequestParameters("q-fiche", defaultParameters, customParameters)
 * 
 * @param {string} type type de la requête Json auprès du serveur
 * @returns {Object}
 */
Scrutari.Api.prototype.buildRequestParameters = function (type) {
    var api = this;
    var requestParameters = {
        "lang": api.lang,
        "warnings": 1,
        "version": 3
    };
    var argLength = arguments.length;
    if (argLength > 1) {
        for(let i = 1; i < argLength; i++) {
            _assign(arguments[i]);
        }
    }
    requestParameters.type = type;
    return requestParameters;
    
    function _assign(parameters) {
        if (!parameters) {
            return;
        }
        for(let propKey in parameters) {
            let propValue = parameters[propKey];
            if (!propValue) {
                propValue = "";
            }
            requestParameters[propKey] = propValue;
        }
    }
    
};

/**
 * @summary Construit un objet ScrutariMeta qui est ensuite transmit à la fonction callback
 * @description
 * Se base sur une requête Json [type=engine]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_engine}
 * 
 * @param {Scrutari.Meta~callback} callback Fonction de retour du chargement de Scrutari.Meta
 * @return {void}
 */
Scrutari.Api.prototype.initMeta = function (callback) {
    var api = this;
    this.loadEngineInfo({
        callback: function (engineInfo) {
            let scrutariMeta = new Scrutari.Meta(engineInfo);
            api.meta = scrutariMeta;
            if (callback) {
                callback(scrutariMeta);
            }
        }
    });
};


/**
 * Objet argument de la méthode loadBaseArray
 * 
 * @typedef Scrutari.Api~baseArrayArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {Scrutari.Ajax~loadBaseArrayCallback} callback fonction de retour traitant le tableau de bases retourné par l'API
 */

/**
 * @summary Requête Json [type=base]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_base}
 * @description
 * Pour les propriétés de l'objet requestParameters, voir la documentation de l'API.
 * 
 * @param {Scrutari.Api~baseArrayArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadBaseArray = function(args) {
    Scrutari.Ajax.loadBaseArray(this, args.requestParameters, args.callback);
};

/**
 * Objet argument de la méthode loadCategoryArray
 * 
 * @typedef Scrutari.Api~categoryArrayArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {Scrutari.Ajax~loadCategoryArrayCallback} callback fonction de retour traitant le tableau de catégories retourné par l'API
 */

/**
 * @summary Requête Json [type=category]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_category}
 * @description
 * Pour les propriétés de l'objet requestParameters, voir la documentation de l'API.
 * 
 * @param {Scrutari.Api~categoryArrayArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadCategoryArray = function(args) {
    Scrutari.Ajax.loadCategoryArray(this, args.requestParameters, args.callback);
};

/**
 * Objet argument de la méthode loadCorpusArray
 * 
 * @typedef Scrutari.Api~corpusArrayArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {Scrutari.Ajax~loadCorpusArrayCallback} callback fonction de retour traitant le tableau de corpus retourné par l'API
 */

/**
 * @summary Requête Json [type=corpus]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_corpus}
 * @description
 * Pour les propriétés de l'objet requestParameters, voir la documentation de l'API.
 * 
 * @param {Scrutari.Api~corpusArrayArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadCorpusArray = function(args) {
    Scrutari.Ajax.loadCorpusArray(this, args.requestParameters, args.callback);
};

/**
 * Objet argument de la méthode loadThesaurusArray
 * 
 * @typedef Scrutari.Api~thesaurusArrayArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {Scrutari.Ajax~loadThesaurusArrayCallback} callback fonction de retour traitant le tableau de thésaurus retourné par l'API
 */

/**
 * @summary Requête Json [type=thesaurus]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_thesaurus}
 * @description
 * Pour les propriétés de l'objet requestParameters, voir la documentation de l'API.
 * 
 * @param {Scrutari.Api~thesaurusArrayArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadThesaurusArray = function(args) {
    Scrutari.Ajax.loadThesaurusArray(this, args.requestParameters, args.callback);
};

/**
 * Objet argument de la méthode loadEngineInfo
 * 
 * @typedef Scrutari.Api~engineInfoArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {Scrutari.Ajax~loadEngineInfoCallback} callback fonction de retour traitant l'objet engineInfo décrivant le moteur
 */

/**
 * @summary Requête Json [type=engine]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_engine}
 * @description
 * Pour les propriétés de l'objet requestParameters, voir la documentation de l'API.
 *
 * @param {Scrutari.Api~engineInfoArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadEngineInfo = function(args) {
    Scrutari.Ajax.loadEngineInfo(this, args.requestParameters, args.callback);
};

/**
 * Objet argument de la méthode loadFicheArray
 * 
 * @typedef Scrutari.Api~ficheArrayArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {Scrutari.Ajax~loadFicheArrayCallback} callback fonction de retour traitant le tableau de fiches retourné par l'API
 */

/**
 * @summary Requête Json [type=fiche]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_fiche}
 * @description
 * Pour les propriétés de l'objet requestParameters, voir la documentation de l'API.
 *
 * @param {Scrutari.Api~ficheArrayArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadFicheArray = function(args) {
    Scrutari.Ajax.loadFicheArray(this, args.requestParameters, args.callback);
};

/**
 * Objet argument de la méthode loadMotcleArray
 * 
 * @typedef Scrutari.Api~motcleArrayArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {Scrutari.Ajax~loadMotcleArrayCallback} callback fonction de retour traitant le tableau de mots-clés retourné par l'API
 */

/**
 * @summary Requête Json [type=motcle]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_motcle}
 * @description
 * Pour les propriétés de l'objet requestParameters, voir la documentation de l'API.
 *
 * @param {Scrutari.Api~motcleArrayArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadMotcleArray = function(args) {
    Scrutari.Ajax.loadMotcleArray(this, args.requestParameters, args.callback);
};

/**
 * Objet argument de la méthode loadGeoJson
 * 
 * @typedef Scrutari.Api~geoJsonArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {Scrutari.Ajax~loadGeoJsonCallback} callback fonction de retour traitant les données au format GeoJson
 * @property {Scrutari.Ajax~errorCallback} errorCallback fonction de traitement des erreurs
 */

/**
 * 
 * @summary Requête Json [type=geojson]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_geojson}
 * @description
 * Effectue une recherche sur les fiches en les renvoyant au format GeoJson (et donc en ne renvoyant que les fiches géolocalisées).
 * 
 * @param {Scrutari.Api~geoJsonArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadGeoJson = function(args) {
    Scrutari.Ajax.loadGeoJson(this, args.requestParameters, args.callback, args.errorCallback);
};

/**
 * Objet argument de la méthode loadFicheSearch
 * 
 * @typedef Scrutari.Api~ficheSearchArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {string} [q] séquence de recherche, peut remplacer requestParameters s'il n'y a pas d'autres éléments à transmettre
 * @property {Scrutari.Ajax~loadFicheSearchResult} callback fonction de retour traitant l'objet ficheSearchResult brut retourné par l'API
 * @property {Scrutari.Ajax~errorCallback} errorCallback fonction de traitement des erreurs
 */

/**
 * @summary Requête Json [type=q-fiche]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_qfiche}
 * @description
 * Lance une nouvelle recherche. Des paramètres q ou q_* sont attendus dans requestParameters. Le paramètre q peut être directement indiqué comme propriété de args.
 * 
 * @param {Scrutari.Api~ficheSearchArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadFicheSearch = function(args) {
    var requestParameters = args.requestParameters;
    if ((!requestParameters) && (args.q)) {
        requestParameters = {
            q: args.q
        };
    }
    Scrutari.Ajax.loadFicheSearchResult(this, args.requestParameters, args.callback, args.errorCallback);
};

/**
 * Objet argument de la méthode loadExistingFicheSearch
 * 
 * @typedef Scrutari.Api~existingFicheSearchArgs
 * @type {Object}
 * @property {Object} requestParameters paramètres de la requête
 * @property {string} [qid] identifiant de la recherche, peut remplacer requestParameters s'il n'y a pas d'autres éléments à transmettre
 * @property {Scrutari.Ajax~loadFicheSearchResult} callback fonction de retour traitant l'objet ficheSearchResult brut retourné par l'API
 */

/**
 * @summary Requête Json [type=q-fiche]{@link http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_qfiche} avec un identifiant de recherche existant (qid)
 * @description
 * Rappelle une recherche existante (en se basant sur le qId). Ceaa permet de compléter un résultat de recherche avec de nouveaux éléments.
 * 
 * @param {Scrutari.Api~existingFicheSearchArgs} args objet argument
 * @returns {void}
 */
Scrutari.Api.prototype.loadExistingFicheSearch = function(args) {
    var requestParameters = args.requestParameters;
    if ((!requestParameters) && (args.qid)) {
        requestParameters = {
            qid: args.qid
        };
    }
    Scrutari.Ajax.loadExistingFicheSearchResult(this, requestParameters, args.callback);
};


