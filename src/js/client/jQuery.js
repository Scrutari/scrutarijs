/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2024 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Regroupe les utilitaires liés à jQuery.
 * N'est pas un constructeur de classe.
 * 
 * @namespace
 * @private
 */
Scrutari.jQuery = {};

/**
 * Cinq signatures :
 * - une chaine unique : recherche de l'élément avec l'identifiant
 * - un objet : transformation de l'objet en sélecteur CSS
 * - deux chaines : concaténation des deux chaines avec _
 * - un premier argument et un objet : conversion de l'objet en sélecteur CSS et recherche 
 * à partir de l'élément décrit par le premier argument
 * - plusieurs chaines : concaténation des chaines avec _
 * 
 * @returns {$} renvoie un objet jQuery
 */
Scrutari.jQuery.find = function () {
    switch(arguments.length) {
        case 0:
            throw new Error("No argument");
        case 1: {
            let uniqueArg = arguments[0];
            if (typeof uniqueArg === "string") {
                return _byId(uniqueArg);
            } else {
                return _byProperties(uniqueArg);
            }
        }
        case 2: {
            let arg1 = arguments[0];
            let arg2 = arguments[1];
            if ((typeof arg1 === "string") && (typeof arg2  === "string")) {
                return _byId(arg1 + "_" + arg2);
            } else {
                return _find(arg1, arg2);
            }
        }
        default:
            return _byId(Array.from(arguments).join("_"));
    }
    
    function _byId(id) {
        return $(document.getElementById(id));
    }
    
    function _byProperties(properties) {
        return $(Scrutari.jQuery.toCssSelector(properties));
    }
    
    function _find(jqArgument, properties) {
        return Scrutari.jQuery.convert(jqArgument).find(Scrutari.jQuery.toCssSelector(properties));
    }
    
};

/**
 * Convertit l'argument en objet JQuery. S'il s'agit déjà d'un objet JQuery, il
 * est simplement retourné.
 * 
 * @param {JQuery|string|object} jqArgument
 * @returns {JQuery} Objet JQuery résultant
 */
Scrutari.jQuery.convert = function (jqArgument) {
    if (jqArgument.jquery) {
        return jqArgument;
    } else {
        return $(jqArgument);
    }
};

/**
 * Vérifie si l'ensemble est non vide.
 * 
 * @param {JQuery|string|object} jqArgument
 * @returns {boolean} si length > 0
 */
Scrutari.jQuery.exists = function (jqArgument) {
    return Scrutari.jQuery.convert(jqArgument).length > 0;
};

Scrutari.jQuery.toCssSelector = function (properties) {
    var query = "";
    var elementName = false;
    var suffix = "";
    for(let key in properties) {
        let value = properties[key];
         if (!key.startsWith("_")) {
             if (value === true) {
                 query += "[" + Scrutari.jQuery.toDataAttribute(key) + "]";
             } else {
                 query += "[" + Scrutari.jQuery.toDataAttribute(key) + "='" + value + "']";
             }
         } else if (key === "_checked") {
             if (value) {
                suffix += ":checked";
            } else {
                suffix += ":not(:checked)";
            }
         } else if (key === "_type") {
             query += "[type='" + value + "']";
         } else if (key === "_name") {
             query += "[name='" + value + "']";
         } else if (key === "_value") {
             query += "[value='" + value + "']";
         } else if (key === "_element") {
             elementName = value;
         }
     }
     if (elementName) {
         query = elementName + query;
     }
     query += suffix;
     return query;
};

Scrutari.jQuery.toDataAttribute = function (camelCaseString) {
    return "data-" + camelCaseString.replace(/[A-Z]/g, function (upperLetter) {
        return "-" + upperLetter.toLowerCase();
    });
};

/**
 * Alias
 * 
 */
Scrutari.jQuery.find.toSelector = Scrutari.jQuery.toCssSelector;

/*
 * Accès direct via la racine Scrutari pour rétrocomptabilité 
 */
Scrutari.toCssQuery = Scrutari.jQuery.toCssSelector;
Scrutari.toDataAttribute = Scrutari.jQuery.toDataAttribute;
Scrutari.convert = Scrutari.jQuery.convert;
Scrutari.exists = Scrutari.jQuery.exists;
Scrutari.$ = Scrutari.jQuery.find;
