# Documentation avec JsDoc

## Installer JsDoc

  npm install -g jsdoc

## Lancer JsDoc

Se rendre dans le répertoire jsdoc/ et lancer ./jsdoc-scrutarijs.sh

Cette commande va créer à la racine un répertoire doc/ (s'il existait avant, il est supprimé et recréé).

Ce répertoire doc/ contient la documentation sous deux versions :

doc/all/ : c'est la documentation complète en suivant le modèle par défaut de JsDoc (appel « jsdoc -c conf-all.json ../src/js -d ../doc/all » dans jsdoc-scrutarijs.sh)

doc/public/ : c'est une documentation personnalisée et simplifiée, limitée aux méthodes publiques

La génération de doc/public/ suit un processus particulier :

- il est d'abord généré un fichier au format XML de la documentation (« jsdoc -c conf-public.json ../src/js > ../doc/public/jsdoc-scrutarijs.xml »)
- puis une transformation XSLT (dont les fichiers sont dans public/xslt) transforme jsdoc-scrutarijs.xml en index.html (« xsltproc -param INCLUDE_DOCTYPE 1 -o ../doc/public/index.html public/xslt/transformation.xsl ../doc/public/jsdoc-scrutarijs.xml »)
- le fichier jsdoc-scrutarijs.xml est ensuite supprimé et le contenu de public/static est copié dans ../doc/public

Pour travailler sur le XSLT, une solution est ne pas supprimer jsdoc-scrutarijs.xmle et de l'ouvrir dans un navigateur en laissant celui-ci faire la transformation XSLT.

