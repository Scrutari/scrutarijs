<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:include href="functions.xsl"/>
    <xsl:output method="html" encoding="UTF-8"/>
    <xsl:param name="INCLUDE_DOCTYPE" select="0"/>
    <xsl:variable name="CONF" select="document('conf.xml')/conf"/>
    
    <xsl:template match="/">
        <xsl:if test="$INCLUDE_DOCTYPE = 1">
            <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;&#10;</xsl:text>
        </xsl:if>
        <html lang="fr">
            <head>
                <title><xsl:value-of select="$CONF/title"/></title>
                <link href="static/jsdoc.css" rel="stylesheet" type="text/css" />
            </head>
            <body>
                <nav>
                    <div>
                        <div><xsl:value-of select="$CONF/title"/></div>
                        <xsl:apply-templates select="jsdoc/namespace[@name='Scrutari']" mode="_Nav"/>
                    </div>
                </nav>
                <main>
                    <xsl:apply-templates select="jsdoc/namespace[@name='Scrutari']" mode="_Main"/>
                </main>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="namespace" mode="_Main">
        <xsl:variable name="Current" select="current()"/>
        <h1><xsl:value-of select="$CONF/title"/></h1>
        <xsl:for-each select="$CONF/chapter">
            <h2 id="{@name}"><xsl:value-of select="@name"/></h2>
            <div class="jsdoc-Text">
                <xsl:value-of select="description" disable-output-escaping="yes"/>
            </div>
            <div class="jsdoc-Chapter">
            <xsl:for-each select="part">
                <xsl:variable name="longname" select="@longname"/>
                <xsl:apply-templates select="$Current/*[@longname=$longname]" mode="_Part"/>
            </xsl:for-each>
            </div>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="namespace" mode="_Nav">
        <ul>
            <xsl:for-each select="$CONF/chapter">
                <li>
                    <a href="#{@name}"><xsl:value-of select="@name"/></a>
                    <ul>
                <xsl:for-each select="part">
                        <li><a href="#{@longname}"><xsl:value-of select="@longname"/></a></li>
                </xsl:for-each>
                    </ul>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    
    <xsl:template match="namespace" mode="_Part">
        <div id="{@longname}">
            <h3><xsl:value-of select="@longname"/></h3>
            <xsl:if test="description">
                <div class="jsdoc-Text">
                    <xsl:value-of select="description" disable-output-escaping="yes"/>
                </div>
            </xsl:if>
            <div class="jsdoc-Class">
                <h4 class="jsdoc-ListTitle">Fonctions statiques</h4>
                <div class="jsdoc-List">
                    <xsl:apply-templates select="function[@scope='static']" mode="_Details">
                        <xsl:sort select="@name"/>
                    </xsl:apply-templates>
                </div>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="class" mode="_Part">
        <div id="{@longname}">
            <h3><xsl:value-of select="../@longname"/>.<xsl:value-of select="@name"/></h3>
            <xsl:if test="description">
                <div class="jsdoc-Text">
                    <xsl:value-of select="description" disable-output-escaping="yes"/>
                </div>
            </xsl:if>
            <div class="jsdoc-Class">
                <xsl:choose>
                    <xsl:when test="@hideconstructor = 'true'">
                        <xsl:if test="function[@scope='static']">
                            <h4 class="jsdoc-ListTitle">Initialisation</h4>
                            <div class="jsdoc-List">
                                <xsl:apply-templates select="function[@scope='static']" mode="_Details"/>
                            </div>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <h4 class="jsdoc-ListTitle">Constructeur</h4>
                        <div class="jsdoc-List">
                            <xsl:apply-templates select="constructor" mode="_Details"/>
                        </div>
                    </xsl:otherwise>
                </xsl:choose>
            <xsl:if test="member">
                <h4 class="jsdoc-ListTitle">Propriétés publiques</h4>
                <table class="member-Table">
                    <tr>
                        <th>Nom</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    <xsl:apply-templates select="member"/>
                </table>
            </xsl:if>
            <xsl:if test="function[@scope='instance']">
                <h4 class="jsdoc-ListTitle">Méthodes</h4>
                <div class="jsdoc-List">
                    <xsl:apply-templates select="function[@scope='instance']" mode="_Details">
                        <xsl:sort select="@name"/>
                    </xsl:apply-templates>
                </div>
            </xsl:if>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="member">
        <tr>
            <td><xsl:value-of select="@name"/></td>
            <td><xsl:apply-templates select="type" mode="_Table"/></td>
            <td>
                <div class="jsdoc-Text">
                    <xsl:value-of select="description" disable-output-escaping="yes"/>
                </div>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="*" mode="_Details">
        <details class="jsdoc-Function">
            <summary>
                <span class="function-Signature">
                    <xsl:apply-templates select="." mode="fn_Signature"/>
                    <xsl:text> </xsl:text>
                    <span class="syntax-Operator">(</span>
                    <xsl:apply-templates select="parameter" mode="fn_Signature"/>
                    <span class="syntax-Operator">)</span>
                </span>
                <xsl:choose>
                    <xsl:when test="summary"> 
                        <span class="function-ShortDescription">
                            <span class="jsdoc-Text">
                                <xsl:value-of select="summary" disable-output-escaping="yes"/>
                            </span>
                        </span>
                    </xsl:when>
                    <xsl:when test="description">
                        <span class="function-ShortDescription">
                            <span class="jsdoc-Text">
                                <xsl:value-of select="description" disable-output-escaping="yes"/>
                            </span>
                        </span>
                    </xsl:when>
                </xsl:choose>
            </summary>
            <div class="function-Description">
                <xsl:if test="summary">
                    <div class="jsdoc-Text">
                        <xsl:value-of select="description" disable-output-escaping="yes"/>
                    </div>
                </xsl:if>
                <xsl:if test="parameter">
                    <table class="jsdoc-Table function-table-Parameters">
                        <caption>Arguments</caption>
                        <tr>
                            <th>Nom</th>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                    <xsl:apply-templates select="parameter" mode="_Table"/>
                    </table>
                </xsl:if>
                <xsl:if test="returns/type[@name != 'void']">
                    <table class="jsdoc-Table function-table-Returns">
                        <caption>Valeur de retour</caption>
                        <tr>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                        <xsl:apply-templates select="returns" mode="_Table"/>
                    </table>
                </xsl:if>
            </div>
        </details>
    </xsl:template>
    
    <xsl:template match="parameter|property" mode="_Table">
        <tr>
            <td><xsl:value-of select="@name"/></td>
            <td><xsl:apply-templates select="type" mode="_Table"/></td>
            <td>
                <div class="jsdoc-Text">
                    <xsl:value-of select="description" disable-output-escaping="yes"/>
                    <xsl:apply-templates select="." mode="_Attributes"/>
                </div>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="returns" mode="_Table">
        <tr>
            <td><xsl:apply-templates select="type" mode="_Table"/></td>
            <td>
                <div class="jsdoc-Text">
                    <xsl:value-of select="description" disable-output-escaping="yes"/>
                    <xsl:apply-templates select="." mode="_Attributes"/>
                </div>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="type" mode="_Table">
        <xsl:variable name="name" select="@name"/>
        <xsl:choose>
            <xsl:when test="//class[@longname=$name]">
                <div>
                    <a href="#{$name}" class="syntax-Type_module">
                        <xsl:value-of select="$name"/><xsl:apply-templates select="." mode="fn_Array"/>
                    </a>
                </div>
            </xsl:when>
            <xsl:when test="//typedef[@longname=$name][type[@name='function']]">
                <xsl:apply-templates select="//typedef[@longname=$name]" mode="_FunctionDetails"/>
            </xsl:when>
            <xsl:when test="//typedef[@longname=$name][type[@name='Object']]">
                <xsl:variable name="withArray"><xsl:if test="@array = 'true'">1</xsl:if></xsl:variable>
                <xsl:apply-templates select="//typedef[@longname=$name]" mode="_ObjectDetails">
                    <xsl:with-param name="withArray" select="$withArray"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="($name='Object') and (../property)">
                <xsl:apply-templates select="." mode="_PropertyDetails"/>
            </xsl:when>
            <xsl:otherwise>
                <div>
                    <xsl:attribute name="class"><xsl:apply-templates select="." mode="fn_Class"/></xsl:attribute>
                    <xsl:value-of select="$name"/><xsl:apply-templates select="." mode="fn_Array"/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="*" mode="_Attributes">
        <xsl:if test="attribute">
            <xsl:text> </xsl:text>
            <span class="jsdoc-Attributes">(<xsl:apply-templates select="attribute"  mode="_Attribute"/>)</span>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="type" mode="_PropertyDetails">
        <details class="typedef-Details">
            <summary>
                <span class="syntax-Type_standard">Object</span>
            </summary>
            <div class="typedef-Description">
                <table class="jsdoc-Table typedef-Table">
                        <caption>Propriétés</caption>
                        <tr>
                            <th>Nom</th>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                    <xsl:apply-templates select="../property" mode="_Table"/>
                </table>
            </div>
        </details>
    </xsl:template>
    
  
    <xsl:template match="typedef" mode="_FunctionDetails">
        <details class="typedef-Details">
            <summary>
                <xsl:apply-templates select="." mode="fn_Signature"/>
                <xsl:text> </xsl:text>
                <span class="syntax-Operator">(</span>
                <xsl:apply-templates select="parameter" mode="fn_Signature"/>
                <span class="syntax-Operator">)</span>
            </summary>
            <div class="typedef-Description">
                <div class="jsdoc-Text">
                    <xsl:value-of select="summary" disable-output-escaping="yes"/>
                    <xsl:value-of select="description" disable-output-escaping="yes"/>
                </div>
                <xsl:if test="parameter">
                    <table class="jsdoc-Table typedef-Table">
                        <caption>Arguments</caption>
                        <tr>
                            <th>Nom</th>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                    <xsl:apply-templates select="parameter" mode="_Table"/>
                    </table>
                </xsl:if>
            </div>
        </details>
    </xsl:template>
    
    <xsl:template match="typedef" mode="_ObjectDetails">
        <xsl:param name="withArray" select="''"/>
        <details class="typedef-Details">
            <summary>
                <span class="syntax-Type_apiobject">
                    <xsl:value-of select="@name"/>
                    <xsl:if test="string-length($withArray) &gt; 0">[]</xsl:if>
                </span>
            </summary>
            <div class="typedef-Description">
                <div class="jsdoc-Text">
                    <xsl:value-of select="summary" disable-output-escaping="yes"/>
                    <xsl:value-of select="description" disable-output-escaping="yes"/>
                </div>
                <xsl:if test="property">
                    <table class="jsdoc-Table typedef-Table">
                        <caption>Propriétés</caption>
                        <tr>
                            <th>Nom</th>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                    <xsl:apply-templates select="property" mode="_Table"/>
                    </table>
                </xsl:if>
            </div>
        </details>
    </xsl:template>
    
    <xsl:template match="attribute" mode="_Attribute">
        <xsl:if test="position() &gt; 1">
            <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:variable name="Name" select="@name"/>
        <xsl:choose>
            <xsl:when test="$Name = 'optional'">facultatif</xsl:when>
            <xsl:when test="($Name = 'nullable') and (@value = 'true')">possiblement nul</xsl:when>
            <xsl:when test="$Name = 'default'"><xsl:text>valeur par défaut = </xsl:text><xsl:value-of select="@value"/></xsl:when>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
