# ScrutariJs, client Javascript de Scrutari

## Compilation

La compilation se fait avec Jake. Pour installer Jake, il faut d'abord installer npm (présent dans toutes les distributions), vérifier que la commande zip fonctionne puis installer Jake avec :

  npm install -g jake

Il faut alors se rendre dans le répertoire racine du projet et taper :

  jake build[$version]

où $version doit être remplacé par le numéro de version (Exemple : jake build[1.0.1])

La compilation va créer un répertoire dist/ et un répertoire pkg/. Le répertoire dist/ comprend les fichiers compilés et le répertoire pkg contient un fichier compressé au format zip du répertoire dist/.

Le répertoire dist/ est vidé à chaque compilation, ce qui n'est pas le cas du répertoire pkg/.


## Téléchargement

Les versions stables de ScrutariJs sont disponibles à partir de la page du wiki [Téléchargement](https://framagit.org/Scrutari/scrutarijs/wikis/Téléchargement)


## Installation

L'installation est décrite sur la page du wiki [Installation](https://framagit.org/Scrutari/scrutarijs/wikis/installation)


## Adaptation

Pour adapter l'interface, la première chose est de surcharger le CSS. Toutes les classes de css/scrutarijs.css sont préfixées par scrutari-, il est donc simple de les remplacer en CSS sans risque d'altération du style existant.

La deuxième étape consiste à reformuler les éléments de structure et les gabarits comme indiqué à la page du wiki [Adaptation](https://framagit.org/Scrutari/scrutarijs/wikis/adaptation)

