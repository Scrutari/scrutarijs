/* global jake,process */
/**
 * Installer Jake avec npm install -g jake (NodeJs doit être installé préalablement)
 * 
 * Lancer la compilation avec jake build[$version] où $version est le numéro de version
 */

var fs = require('fs')
   , exec = require('child_process').exec;

task('build', function (version) {
    jake.rmRf('dist');
    jake.mkdirP('dist');
    jake.mkdirP('pkg');
    compileJs(version, 'scrutarijs-api.js', 'list-api.txt');
    compileJs(version, 'scrutarijs.js', 'list.txt');
    compileL10n();
    copyCss('scrutarijs.css');
    compileFrameworks();
    zip(version);
});

task('default', ['build']);

/**
 * Compile les différents fichiers Javascript suivant l'ordre indiqué dans list.txt
 * en un fichier Javascript unique
 * Les fichiers Javascript sont vidés de leurs commentaires et des espaces blancs surnuméraires.
 * 
 * @param {string} version version de la compilation
 * @param {string} destination nom du fichier de destination
 * @param {string} list nom du fichier avec la liste des fichiers à compiler
 * @returns {void}
 */
function compileJs(version, destination, list) {
    var files = fs.readFileSync(list, 'utf8').split("\n");
    var result = "/* version: " + version + " */\n";
    for (var i = 0, len = files.length; i < len; i++) {
        var fileName = files[i];
        if (fileName.length > 0) {
            var contents = fs.readFileSync("src/js/" + fileName, 'utf8');
            contents = contents.replace(/\/\*[\s\S]*?\*\//g,'');
            result = result + contents;
            console.log(files[i]);
        }
    }
    result = result.replace(/\n\s*\n/g, "\n");
    fs.writeFileSync('dist/' + destination, result, 'utf8');
}

/**
 * Liste les langues présentes dans src/l10n et effectue leur compilation.
 * Chaque langue est un répertoire donné qui contient des fichiers .ini (pour les simples chaines)
 * ou .html (pour les textes avec mise en forme comme les dialogues d'aide).
 * Ces différents fichiers sont compilés dans un fichier Javascript au sein d'un objet unique nommé
 * SCRUTARI_L10N
 * 
 * @returns {void}
 */
function compileL10n() {
    jake.mkdirP('dist/l10n');
    var langDirs = fs.readdirSync('src/l10n');
    for(let lang of langDirs) {
        if ((fs.statSync('src/l10n/' + lang).isDirectory()) && (lang.match(/^[-a-zA-Z_]+$/))) {
            _compileLang(lang);
        }
    }
    
    
    function _compileLang(lang) {
        console.log(lang);
        let dirPath = 'src/l10n/' + lang + '/';
        let files = fs.readdirSync(dirPath);
        let result = "var SCRUTARI_L10N = {\nlang:'" + lang + "'";
        for(let fileName of  files) {
            if (fileName.substr(-4) === ".ini") {
                result += _convertIniFile(dirPath + fileName);
            }
        }
        for(let fileName of  files) {
            if (fileName.substr(-5) === ".html") {
                let key = fileName.substring(0, fileName.length - 5);
                result += _convertHtmlFile(key, dirPath + fileName);
            }
        }
        result += "\n};\n";
        fs.writeFileSync('dist/l10n/' + lang + '.js', result, 'utf8');
    }
    
    function _convertIniFile(iniFilePath) {
        let result = "";
        let lines = fs.readFileSync(iniFilePath, 'utf8').split("\n");
        for(let line of lines) {
            line = line.trim();
            if (line.length === 0) {
                continue;
            }
            let firstChar = line.substring(0,1);
            let ignore = false;
            switch(firstChar) {
                case ';':
                case '#':
                case '!':
                case '[':
                    ignore = true;
            }
            if (ignore) {
                continue;
            }
            let idx = line.indexOf('=');
            if (idx < 1) {
                continue;
            }
            let key = line.substring(0,idx).trim();
            let value = line.substring(idx+1).trim();
            result += ",\n'" + key + "':'";
            result += value.replace(/'/g, "\\'");
            result += "'";
        }
        return result;
    }
    
    function _convertHtmlFile(key, htmlFilePath) {
        let html = fs.readFileSync(htmlFilePath, 'utf8');
        let result = ",\n'_ " + key + ".html':'";
        result += html.replace(/\s+/g, ' ').replace(/'/g, "\\'");
        result += "'";
        return result;
    }
    
}


/**
 * Copie le fichier CSS commun ainsi que les images
 * 
 * @param {string} cssFileName nom du fichier à copier
 * @returns {void}
 */
function copyCss(cssFileName) {
    jake.cpR('src/css/' + cssFileName, "dist");
    jake.cpR('src/css/images', "dist");
}

/**
 * Compile les différents frameworks listés dans src/frameworks/. Les frameworks
 * sont des répertoires contenant les sous-répertoires (optionnels) suivants :
 * - structure
 * - templates
 * - resources
 * ainsi que les deux fichiers : framework.css et framework.js (optionnel)
 * La compilation convertit les différents contenus de structures/ et template/ en propriétés d'un objet Javascript SCRUTARI_HTML contenu dans fichier appelé {nom du framework}.js
 * auquel est adjoint le contenu de framework.js.
 *  framework.css est copié et renommé {nom du framework}.css. Le répertoire resources est copié et renommé {nom du framework}/.
 * 
 * Le contenu du framework default est inséré
 * dans {nom du framework}.js sauf si le framework contient un fichier de même nom.
 * 
 * La compilation construit également un fichier {nom du framework}.html,
 * ce fichier n'est pas utilisé par l'application, il sert à des fins d'analyse pour indiquer
 * les inclusions successives des fichiers des répertoires structure/
 * 
 * @returns {void}
 */
function compileFrameworks() {
    jake.mkdirP('dist/frameworks');
    var frameworkDirs = fs.readdirSync('src/frameworks');
    var defaultPath = 'src/frameworks/default/';
    for(let frameworkName of frameworkDirs) {
        if ((fs.statSync('src/frameworks/' + frameworkName).isDirectory()) && (frameworkName.match(/^[-0-9a-zA-Z_]+$/))) {
            _compileJavascriptFile(frameworkName);
            jake.cpR('src/frameworks/' + frameworkName + '/framework.css', 'dist/frameworks/' + frameworkName + '.css');
            let resourcesPath = 'src/frameworks/' + frameworkName + '/resources';
            if (fs.existsSync(resourcesPath)) {
                jake.cpR(resourcesPath, 'dist/frameworks/' + frameworkName);
            }
            _buildStructure(frameworkName);
            console.log(frameworkName);
        }
    }
    
    
    function _compileJavascriptFile(frameworkName) {
        let isDefault = (frameworkName === 'default');
        let frameworkPath = 'src/frameworks/' + frameworkName + '/';
        let result = "var SCRUTARI_HTML = {\n_name:'" + frameworkName + "'";
        result += ",\nstructure:{\n";
        let structureExistingArray = new Array();
        if (!isDefault) {
            result += _compileFrameworkDir(frameworkPath + 'structure/', structureExistingArray);
        }
        result += _compileFrameworkDir(defaultPath + 'structure/', structureExistingArray);
        result += "\n}";
        result += ",\ntemplates:{\n";
        let templatesExistingArray = new Array();
        if (!isDefault) {
            result += _compileFrameworkDir(frameworkPath + 'templates/', templatesExistingArray);
        }
        result += _compileFrameworkDir(defaultPath + 'templates/', templatesExistingArray);
        result += "\n}";
        result += "\n};";
        let frameworkJavascriptPath = frameworkPath + 'framework.js';
        if (fs.existsSync(frameworkJavascriptPath)) {
            result += "\n\n" + fs.readFileSync(frameworkJavascriptPath, 'utf8');
        }
        fs.writeFileSync('dist/frameworks/' + frameworkName + '.js', result, 'utf8');
    }
    
    function _compileFrameworkDir(path, existingArray) {
        if (!fs.existsSync(path)) {
            return "";
        }
        let files = fs.readdirSync(path);
        let result = "";
        for(let fileName of files) {
            if (fileName.substr(-5) === ".html") {
                let key = fileName.substring(0, fileName.length - 5);
                if (existingArray.indexOf(key) === -1) {
                    result += _convertHtmlFile(key, path + fileName, (existingArray.length > 0));
                    existingArray.push(key);
                }
            }
        }
        return result;
    }

    function _convertHtmlFile(key, htmlFilePath, next) {
        let html = fs.readFileSync(htmlFilePath, 'utf8');
        let result = "";
        if (next) {
            result += ",\n";
        }
        result += "'" + key + "':'";
        let lines = html.split('\n');
        let first = true;
        for(let line of lines) {
            line = line.replace(/\t/g, '    ').replace(/{{!--.*--}}/g, "").replace(/'/g, "\\'");
            if (line.trim().length === 0) {
                continue;
            }
            if (first) {
                first = false;
            } else {
                result += "\\n";
            }
            result += line;
        }
        result += "'";
        return result;
    }
    
    function _buildStructure(frameworkName) {
        let frameworkPath = 'src/frameworks/' + frameworkName + '/';
        let includedTexts = {};
        fs.writeFileSync('dist/frameworks/' + frameworkName + '.html', __getText("main", "", "/"), 'utf8');
 
 
        function __getText(name, indent, parent) {
            let fileContent = __getFileContent(name);
            let text = indent + "<!-- {{" + name + "}}" + " (" + parent + ")" + " (" + fileContent.origin + ")" + " -->\n";
            if (fileContent.text.length > 0) {
                let lines = fileContent.text.split("\n");
                 for(let line of lines) {
                    if (line.trim().length > 0) {
                        let idx = line.indexOf("{{");
                        if (idx !== -1) {
                            let indent2 = indent + __getIndent(idx);
                            line = line.trim().replace(/{{([-a-zA-z0-9_]+)}}/g, function (match, p1) {
                                return __getText(p1, indent2, parent + name + "/");
                            });
                            text += line;
                        } else {
                            text += indent + line + "\n";
                        }
                    }
                }
            }
            text += indent + "<!-- {{/" + name + "}} -->\n";
            return text;
        }
        
        function __getFileContent(name) {
            if (includedTexts.hasOwnProperty(name)) {
                return {
                    origin: "duplicate",
                    text: ""
                };
            } else {
                includedTexts[name] = true;
            }
            let path = frameworkPath + 'structure/' + name + '.html';
            let origin = (frameworkPath.indexOf("default/") > -1)?"default":"framework";
            let text = "";
            if (!fs.existsSync(path)) {
                origin = "default";
                path = defaultPath + 'structure/' + name + '.html';
            }
            if (fs.existsSync(path)) {
                text = fs.readFileSync(path, 'utf8');
            } else {
               origin = "unknown";
            }
            return {
                origin: origin,
                text: text
            };
        }
        
        function __getIndent(number) {
            let text = "";
            for(let i = 0; i < number; i++) {
                text += " ";
            }
            return text;
        }

    }
}


/**
 * Compresse l'ensemble des fichiers dans une archive au format zip
 * 
 * @param {string} version version de la compilation
 * @returns {void}
 */
function zip(version) {
    var cmd = "zip -r ../pkg/scrutarijs-" + version + ".zip *";
    var currDir = process.cwd();
    process.chdir('dist');
    exec(cmd, function (err, stdout, stderr) {
          if (err) { throw err; }
          process.chdir(currDir);
      });
}