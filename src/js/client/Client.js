/* global Scrutari,SCRUTARI_L10N,SCRUTARI_HTML */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * @classdec
 * Objet centralisant toutes les opérations sur le client Javascript
 * 
 * 
 * @class
 * @param {Scrutari.Api} api
 * @param {string} clientId
 * @hideconstructor
 * 
 * @property {Scrutari.Api} api - Api d'accsè au moteur Scrutari
 * @property {string} clientId - Identifiant HTML du bloc conteneur du client
 * @property {Scrutari.Meta} scrutariMeta - Métadonnées du moteur
 * @property {Scrutari.History} history - Historique des recherches effectuées
 * @property {Scrutari.Stats} stats - Statistique du moteur
 */
Scrutari.Client = function (api, clientId) {
    this.api = api;
    this.scrutariConfig = api;
    this.clientId = clientId;
    this.history = new Scrutari.History(this);
    this.scrutariMeta = null;
    this.completor = null;
    this.stats = null;
    this.isWaiting = false;
    this.options = {
        withCorpus: false,
        baseSort: "fiche-count",
        corpusSort: "fiche-count",
        categorySort: "rank",
        langSort: "code",
        initialQuery: "",
        initialQId: "",
        initialFilters: null,
        permalinkPattern: null,
        ficheTarget: "_blank",
        ignoreIcon: false,
        ignoreThumbnail: false,
        ficheBodyList: null,
        mainTitle: null,
        historyAtLast: false,
        hooks: {},
        functions: {
            uiInit: null
        },
        cssClasses: {
        },
        structureOptions: {            
        }
    };
    this.functions = {
        actionHandler: Scrutari.DefaultUi.actionHandler,
        buildSearchRequestParameters: Scrutari.DefaultUi.buildSearchRequestParameters,
        buildFilterState: Scrutari.DefaultUi.buildFilterState,
        changeHandler: Scrutari.DefaultUi.changeHandler,
        checkInitialQuery: Scrutari.DefaultUi.checkInitialQuery,
        displayError: Scrutari.DefaultUi.displayError,
        endLoading: Scrutari.DefaultUi.endLoading,
        ignoreElement: Scrutari.DefaultUi.ignoreElement,
        initFilterByQuery: Scrutari.DefaultUi.initFilterByQuery,
        initForms: Scrutari.DefaultUi.initForms,
        isFilterEnable: Scrutari.DefaultUi.isFilterEnable,
        modalAction: Scrutari.DefaultUi.modalAction,
        newSearchDisplay: Scrutari.DefaultUi.newSearchDisplay,
        startLoading: Scrutari.DefaultUi.startLoading
    };
    this.searchCallbacks = {
        mainsearch: Scrutari.DefaultUi.mainsearchCallback,
        qidsearch: Scrutari.DefaultUi.qidsearchCallback,
        subsearch: Scrutari.DefaultUi.subsearchCallback 
    };
    this.errorCallbacks = {
        mainsearch: null,
        qidsearch: null,
        subsearch: null
    };
    this.cssClasses = {
        disabled: "scrutari-Disabled",
        excluded: "scrutari-Excluded",
        hidden: "scrutari-Hidden",
        mark: "scrutari-Mark",
        on: "scrutari-On",
        activeHistory: "scrutari-history-Active",
        activePanel: "scrutari-panel-Active",
        filterStat: "scrutari-stats-Filter",
        noneStat: "scrutari-stats-None"
    };
    this._locInstance = new Scrutari.Loc();
    this._ignoreArray = new Array();
    this._ficheBodyTemplateArray = [
        "fiche_mtitre",
        "fiche_msoustitre",
        "fiche_year",
        "fiche_primaryattributearray",
        "fiche_mcomplementarray",
        "fiche_secondaryattributearray",
        "fiche_motclearray",
        "fiche_bythesaurusarray"
    ];
    this._structureMap = new Map();
    this._templateMap = new Map();
    this._paginationMap = new Map();
    
    if (typeof SCRUTARI_L10N !== 'undefined') {
        this._locInstance.putAll(SCRUTARI_L10N);
    }
    if (typeof SCRUTARI_HTML !== 'undefined') {
        this._htmlObject = SCRUTARI_HTML;
    }
};

/**
 * 
 * @param {string} locKey
 * @returns {string}
 */
Scrutari.Client.prototype.loc = function (locKey) {
    return this._locInstance.loc(locKey);
};

/**
 * 
 * @param {string} qId
 * @returns {string}
 */
Scrutari.Client.prototype.toPermalink = function (qId) {
    if (this.options.permalinkPattern) {
        return this.api.getPermalinkUrl(qId, this.options.permalinkPattern);
    }
    return null;
};

/**
 * 
 * @param {string} elementName
 * @returns {boolean}
 */
Scrutari.Client.prototype.ignoreElement = function (elementName) {
    for(let ignorable of this._ignoreArray) {
        if (ignorable.mode === "start") {
            if (elementName.startsWith(ignorable.token)) {
                return true;
            }
        } else {
            if (elementName === ignorable.token) {
                return true;
            }
        }
    }
    if (this._ignoreArray.indexOf(elementName) > -1) {
        return true;
    }
    if (this.functions.ignoreElement) {
        if (this.functions.ignoreElement(elementName)) {
            return true;
        }
    }
    return false;
};

/**
* @param {string} templateName
* @returns {function}
*/
Scrutari.Client.prototype.getTemplate = function (templateName) {
    var template = this.hook("getTemplate", templateName);
    if (template) {
        if (typeof template === "function") {
            return template;
        } else {
            Scrutari.log("getTemplate hook does not return function for template name: " + templateName);
        }
    }
    if (templateName.indexOf(":") === -1) {
        templateName = "scrutari:" + templateName;
    }
    template = $.templates[templateName];
    if (!template) {
        return function () {
            return "Unknown template : " + templateName;
        };
    } else {
        return template;
    }
};

/**
 * 
 * @param {string} templateName
 * @param {Object} context
 * @param {Object} helpers
 * @returns {string}
 */
Scrutari.Client.prototype.render = function (templateName, context, helpers) {
    var templateFunction = this.getTemplate(templateName);
    return templateFunction(context, helpers);
};

/**
 * 
 * @param {string} name
 * @returns {void}
 */
Scrutari.Client.prototype.hook = function (name) {
    var hook = _getHook(this.options.hooks, name);
    if (!hook) {
        return;
    }
    if (typeof hook === "function") {
        var newArgs = new Array();
        var argLength = arguments.length;
        if (argLength > 1) {
            for(let i = 1; i < argLength; i++) {
                newArgs.push(arguments[i]);
            }
        }
        hook.apply(this, newArgs);
    }
    
    function _getHook(hooks, name) {
        if (hooks.hasOwnProperty(name)) {
            return hooks[name];
        }
        let alias;
        switch(name) {
            case "beforeSearchDisplay":
                alias = ["showResult", "afterResultProcess"];
                break;
            case "afterSearchDisplay":
                alias = ["beforeResultProcess"];
                break;
            case "afterPaginationChange":
                alias = ["paginationChange"];
                break;
        }
        if (alias) {
            for (let al of alias) {
                if (hooks.hasOwnProperty(al)) {
                    return hooks[al];
                }
            }
        }
        return false;
    }
};

Scrutari.Client.prototype.getFicheBodyTemplateArray = function (fiche, ficheGroupName) {
    var array = this.hook("getFicheBodyTemplateArray", fiche, ficheGroupName);
    if (array) {
        return array;
    } else {
        return this._ficheBodyTemplateArray;
    }
};

Scrutari.Client.prototype.startOverlay = function (settings) {
    return Scrutari.Overlay.start(settings);
};

Scrutari.Client.prototype.endOverlay = function (overlayId, callback) {
    return Scrutari.Overlay.end(overlayId, callback);
};

Scrutari.Client.prototype.$ = function (properties) {
    var $client = Scrutari.jQuery.find(this.clientId);
    return Scrutari.jQuery.find($client, properties);
};

/**
 * 
 * @param {string} name
 * @param {string} action
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$area = function (name, action) {
    var $area = this.$({scrutariArea: name});
    if (action) {
        switch(action) {
            case 'show':
                this.show($area);
                break;
            case 'hide':
                this.hide($area);
                break;
        }
    }
    return $area;
};

/**
 * 
 * @param {string} name
 * @param {string} action
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$block = function (name, suffix) {
    var $block;
    if (suffix === '*') {
        $block = this.$({"scrutariBlock|": name});
    } else if (suffix) {
        $block = this.$({scrutariBlock: name + "-" + suffix});
    } else {
        $block = this.$({scrutariBlock: name});
    }
    return $block;
};

Scrutari.Client.prototype.$action = function (name, target) {
    if (target) {
        return this.$({scrutariAction: name, scrutariTarget: target});
    } else {
        return this.$({scrutariAction: name});
    }
};

/**
 * 
 * @param {JQuery} $parent
 * @param {string} name
 * 
 * @param {string} value
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$component = function ($parent, name) {
    return Scrutari.jQuery.find($parent, {scrutariComponent: name});
};

/**
 * @summary Retourne les éléments avec l'attribut data-scrutari-count à la valeur name
 * 
 * @description
 * L'argument facultatif action applique une action sur ces éléments.
 * La valeur possible est update qui donne la valeur value à un élement enfant
 * possédant l'attribut data-scrutari-component='value'.
 * 
 * @param {string} name - valeur de data-scrutari-count à sélectionner
 * @param {string} [action] - action à effectuer sur ces élements.
 * @param {string} [value] - valeur à appliquer 
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$count = function (name, action, value) {
    var $count = this.$({scrutariCount: name});
    if (action) {
        switch(action) {
            case 'update':
                if (!value) {
                    value = 0;
                }
                this.$component($count, "value").html(this.formatNumber(value));
                break;
        }
    }
    return $count;
};

/**
 * 
 * @param {string} name
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$form = function (name) {
    return this.$({scrutariForm: name});
};

/**
 * @summary Retourne les éléments avec l'attribut data-scrutari-hidden à la valeur name
 * 
 * @description
 * L'argument facultatif action applique une action sur ces éléments.
 * Les valeurs possibles sont show et hide
 * 
 * @param {string} name - valeur de data-scrutari-hidden à sélectionner
 * @param {string} [action] - action à effectuer sur ces élements.
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$hidden = function (name, action) {
    var $hidden = this.$({scrutariHidden: name});
    if (action) {
        switch(action) {
            case 'show':
                this.show ($hidden);
                break;
            case 'hide':
                this.hide($hidden);
                break;
        }
    }
    return $hidden;
};

/**
 * 
 * @param {string} name
 * @param {string} value
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$input = function (name, value) {
    if (value) {
        return this.$({_element: "input", _name: name, _value: value});
    } else {
        return this.$({_element: "input", _name: name});
    }
};

/**
 * 
 * @param {string} name
 * @param {string} value
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$input_checked = function (name, value) {
    if (value) {
        return this.$({_element: "input", _name: name, _value: value, _checked: true});
    } else {
        return this.$({_element: "input", _name: name, _checked: true});
    }
};

/**
 * 
 * @param {string} name
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$label = function (name) {
    return this.$({scrutariLabel: name});
};

/**
 * 
 * @param {string} name
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$link = function (name) {
    return this.$({scrutariLink: name});
};

/**
 * 
 * @param {string} name
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$modal = function (name) {
    return Scrutari.jQuery.find({scrutariModal: name});
};

/**
 * 
 * @param {string} name
 * @param {string} action
 * @returns {JQuery}
 */
Scrutari.Client.prototype.$panel = function (name, action) {
    var $panel = this.$({scrutariPanel: name});
    if (action) {
        switch(action) {
            case 'show':
                this.show ($panel);
                break;
            case 'hide':
                this.hide($panel);
                break;
        }
    }
    return $panel;
};

/**
 * 
 * @param {JQuery} jqArgument
 * @returns {void}
 */
Scrutari.Client.prototype.show  = function (jqArgument) {
    var $elements = Scrutari.jQuery.convert(jqArgument);
    $elements.removeClass(this.cssClasses.hidden);
};

/**
 * 
 * @param {JQuery} jqArgument
 * @returns {void}
 */
Scrutari.Client.prototype.hide = function (jqArgument) {
    var $elements = Scrutari.jQuery.convert(jqArgument);
    $elements.addClass(this.cssClasses.hidden);
};

/**
 * 
 * @returns {void}
 */
Scrutari.Client.prototype.scrollToResult = function () {
    $(window).scrollTop(this.$area('result').offset().top);
};

/**
 * 
 * @param {string} name
 * @returns {string}
 */
Scrutari.Client.prototype.getStructureHtml = function (name) {
    if (this._structureMap.has(name)) {
        return this._structureMap.get(name);
    } else {
        Scrutari.log("Unknown structure: " + name);
        return "";
    }
};

Scrutari.Client.prototype.getStructureNameArray = function () {
    var result = new Array();
    for(let key of this._structureMap.keys()) {
        result.push(key);
    }
    return result;
};

Scrutari.Client.prototype.compileStructure = function (name, options) {
    var client = this;
    var includedTexts = new Map();
    var noLoc = false;
    var withComment = false;
    if (options) {
        noLoc = options.noLoc;
        withComment = options.withComment;
    }
    var html = _getText(name, "", "/");
    if (!noLoc) {
        html = html.replace(/_ [-_\.a-z0-9]+/g, function(match) {
            return client.loc(match);
        });
    }
    return html;
    
    function _getText(name, indent, parent) {
        if (name.endsWith('*')) {
            return _getTextByPrefix(name.substring(0, name.length - 1), indent, parent);
        }
        if (includedTexts.has(name)) {
            Scrutari.log("Already included: " + name);
            return "";
        } else if (client.ignoreElement(name)) {
            return "";
        }
        includedTexts.set(name, true);
        let result = "";
        if (withComment) {
            result += indent + "<!-- {{" + name + "}}" + " (" + parent + ")" + " -->\n";
        }
        let text = client.getStructureHtml(name);
        if (text.length > 0) {
            let lines = text.split("\n");
            for(let line of lines) {
                if (line.trim().length === 0) {
                    continue;
                }
                let idx = line.indexOf("{{");
                if (idx !== -1) {
                    let includedText = line.replace(/([ \t]*){{([-a-zA-z0-9_]+\*?)}}/g, function (match, p1, p2) {
                        let includeIndent = indent + p1;
                        return _getText(p2, includeIndent, parent + name + "/");
                    });
                    result += includedText;
                    if ((includedText.length > 0) && (!includedText.endsWith("\n"))) {
                        result += "\n";
                    }
                } else {
                    result += indent + line + "\n";
                }
            }
        }
        if (withComment) {
            result += indent + "<!-- {{/" + name + "}} -->\n";
        }
        return result;
    }
    
    function _getTextByPrefix(namePrefix, indent, parent) {
        let result = "";
        for(let name of client.getStructureNameArray()) {
            if (name.startsWith(namePrefix)) {
                result = result + _getText(name, indent, parent);
            }
        }
        return result;
    }

};

Scrutari.Client.prototype.checkInitialQuery = function () {
    if (this.functions.checkInitialQuery) {
        this.functions.checkInitialQuery(this);
    } 
};

Scrutari.Client.prototype.initForms = function () {
    if (this.functions.initForms) {
        this.functions.initForms(this);
    } 
};

Scrutari.Client.prototype.initActions = function () {
    var client = this;
    Scrutari.jQuery.find(client.clientId).on("click", "[data-scrutari-action]", function () {
        let actionHandler;
        if (client.functions.actionHandler) {
            actionHandler = client.functions.actionHandler;
        } else {
            actionHandler = _noneHandler;
        }
        let button = this;
        let action = this.dataset.scrutariAction;
        let target = this.dataset.scrutariTarget;
        let done = actionHandler(client, button, action, target);
        if (!done) {
            switch(action) {
                case 'showModal':
                    if (client.functions.modalAction) {
                        client.functions.modalAction(client, target, 'show');
                    }
                    break;
                case 'removeHistory':
                    client.history.removeHistory(target);
                    break;
                case 'loadHistory':
                    client.history.loadHistory(target);
                    break;
                case 'clearHistory':
                    client.history.clear();
                    break;
                case 'ficheGroupTab':
                    _ficheGroupTab(target);
                    break;
                case 'paginationTab':
                     _paginationTab(button);
                    break;
                default:
                    Scrutari.log("Unknown action: " + action);
            }
        }
        if ((this.tagName) && (this.tagName.toLowerCase()  === 'a')) {
            return false;
        }
    });
    
    function _noneHandler() {
        return false;
    }
    
    function _ficheGroupTab (target) {
        let activeBlockName = "ficheGroup" + "-" + target;
        client.$block("ficheGroup", "*").each(function (index, element) {
            if (element.dataset.scrutariBlock === activeBlockName) {
                client.show(element);
            } else {
                client.hide(element);
            }
        });
        client.$action("ficheGroupTab").each(function (index, element) {
            element.disabled = (element.dataset.scrutariTarget === target);
        });
    }
    
    function _paginationTab (button) {
        let newPaginationNumber = parseInt(button.dataset.scrutariNumber);
        let pagination = client.getPagination(button.dataset.scrutariFicheGroupName);
        if (pagination) {
            pagination.change(newPaginationNumber);
            if (button.dataset.scrutariPosition === "bottom") {
                 client.scrollToResult();
            }
        }
    }
};

Scrutari.Client.prototype.initChangeListeners = function () {
    var client = this;
    Scrutari.jQuery.find(this.clientId).on("input", "input", function () {
        let changeHandler;
        if (client.functions.changeHandler) {
            changeHandler = client.functions.changeHandler;
        } else {
            changeHandler = _noneHandler;
        }
        let element = this;
        let name = this.name;
        if (name) {
            let done = changeHandler(client, element, name);
            if (!done) {
                switch(name) {
                    case 'q-mode':
                        if (this.value === 'operation') {
                            Scrutari.Utils.disable("input[name='wildchar']");
                        } else {
                            Scrutari.Utils.enable("input[name='wildchar']");
                        }
                        break;
                }
            }
        }
    });
    
    function _noneHandler() {
        return false;
    }
};

Scrutari.Client.prototype.searchCallback = function (searchUnit) {
    var client = this;
    client.displaySearchUnit(searchUnit, true);
    client.endLoading();
    if (client.searchCallbacks.hasOwnProperty(searchUnit.searchOrigin)) {
        client.searchCallbacks[searchUnit.searchOrigin](client, searchUnit);
    }
};

Scrutari.Client.prototype.errorCallback = function (error, searchOrigin) {
    var client = this;
    if (client.functions.displayError) {
        client.functions.displayError(client, error, searchOrigin);
    }
    client.endLoading();
    if (client.errorCallbacks.hasOwnProperty(searchOrigin)) {
        client.errorCallbacks[searchOrigin](client, error);
    }
};

/**
 * Construit les paramètres de requête de recherche à partir des filtres activés.
 * 
 * @param {Object} customParameters Paramètres supplémentaires
 * @returns {Object}
 */
Scrutari.Client.prototype.buildSearchRequestParameters = function (customParameters) {
    if (this.functions.buildSearchRequestParameters) {
        return this.functions.buildSearchRequestParameters(this, customParameters);
    } else if (customParameters) {
        return Object.assign({}, customParameters);
    } else {
        return {};
    }    
};

/**
 * 
 * @returns {Scrutari.FilterState}
 */
Scrutari.Client.prototype.buildFilterState = function () {
    if (this.functions.buildFilterState) {
        return this.functions.buildFilterState(this);
    } else {
        return new Scrutari.FilterState();
    }
};

/**
 * 
 * @param {string} name
 * @returns {string}
 */
Scrutari.Client.prototype.getTemplateHtml = function (name) {
    if (this._templateMap.has(name)) {
        return this._templateMap.get(name);
    } else {
        Scrutari.log("Unknown template: " + name);
        return "";
    }
};

/**
 * 
 * @param {number} number
 * @returns {string}
 */
Scrutari.Client.prototype.formatNumber = function (number) {
    if (Number.prototype.toLocaleString) {
        return number.toLocaleString(this.api.lang);
    } else {
        return number;
    }
};

/**
 * 
 * @param {Scrutari.SearchUnit} searchUnit
 * @param {Scrutari~FicheApiObject} fiche fiche à compléter
 * @param {string} categoryName nom de la catégorie de la fiche
 * @returns {Scrutari~FicheApiObject} fiche modifiée
 */
Scrutari.Client.prototype.completeFiche = function(searchUnit, fiche, ficheGroupName) {
    if (fiche._isCompleted) {
        return fiche;
    }
    var client = this;
    var completor = client.completor;
    var options = client.options;
    completor.complementTitle(fiche);
    completor.motcleArray(fiche, _motcleProvider);
    completor.bythesaurusArray(fiche, _motcleProvider);
    if (Scrutari.hasMarkedAttribute(fiche)) {
        completor.markedAttributeArray(fiche, "primary");
        completor.markedAttributeArray(fiche, "secondary");
    }
    if (!options.ignoreThumbnail) {
        completor.thumbnail(fiche);
    }
    if (!options.ignoreIcon) {
        completor.icon(fiche);
    }
    if (options.ficheTarget) {
        completor.target(fiche, options.ficheTarget);
    }
    fiche._bodyTemplateArray = client.getFicheBodyTemplateArray(fiche, ficheGroupName);
    fiche._isCompleted = true;
    client.hook("completeFiche", searchUnit, fiche, ficheGroupName);
    return fiche;
    
    
    function _motcleProvider(code) {
        return searchUnit.getMotcle(code);
    }
};

/**
 * 
 * @param {string} ficheGroupName nom du groupe de la fiche
 * @returns {Scrutari.Pagination} instance de pagination correspondante
 */
Scrutari.Client.prototype.getPagination = function (ficheGroupName) {
    if (!ficheGroupName) {
        ficheGroupName = "";
    }
    return this._paginationMap.get(ficheGroupName);
};

/**
 * 
 * @param {string} ficheGroupName nom du groupe de la fiche
 * @param {Scrutari.Pagination} pagination instance de pagination correspondante
 * @returns {void}
 */
Scrutari.Client.prototype.putPagination = function (ficheGroupName, pagination) {
    if (!ficheGroupName) {
        ficheGroupName = "";
    }
    this._paginationMap.set(ficheGroupName, pagination);
};

/**
 * 
 * @returns {void}
 */
Scrutari.Client.prototype.clearPagination = function () {
    this._paginationMap.clear();
};

Scrutari.Client.prototype.displaySearchUnit = function (searchUnit, addToHistory) {
    var client = this;
    var searchDisplay;
    if (client.functions.newSearchDisplay) {
        searchDisplay = client.functions.newSearchDisplay(client, searchUnit);
    }
    if (!searchDisplay) {
        searchDisplay = new Scrutari.SearchDisplay(client, searchUnit);
    }
    if (addToHistory) {
        client.history.addSearchUnit(searchUnit);
    }
    searchDisplay.run();
};

Scrutari.Client.prototype.startLoading = function () {
    if (this.functions.startLoading) {
        this.functions.startLoading(this);
    }
};

Scrutari.Client.prototype.endLoading = function () {
    if (this.functions.endLoading) {
        this.functions.endLoading(this);
    }
};

/**
 * 
 * @param {Scrutari.Api} api
 * @param {string} clientId
 * @param {Object} options
 * @param {Scrutari.Client~callback} callback
 * @returns {void}
 */
Scrutari.Client.init = function (api, clientId, options, callback) {
    if (!$.templates) {
        throw new Error("JsRender is not installed");
    }
    var uiInitFunction = Scrutari.DefaultUi.init;
    api.initMeta(function (scrutariMeta) {
        var client = new Scrutari.Client(api, clientId);
        client.scrutariMeta = scrutariMeta;
        client.completor = new Scrutari.Completor(scrutariMeta);
        client.stats = new Scrutari.Stats(scrutariMeta);
        _checkOptions(client, scrutariMeta);
        _initHelpers(client);
        _initMaps(client);
        uiInitFunction(client);
        if (callback) {
            callback(client);
        }
    });
    
    
    function _checkOptions (client, scrutariMeta) {
        var defaultOptions = scrutariMeta.getDefaultOptions();
        for(let key in defaultOptions) {
            client.options[key] = defaultOptions[key];
        }
        if (options) {
            for(let key in options) {
                client.options[key] = options[key];
            }
            if (options.functions) {
                for(let functionKey in options.functions) {
                    switch(functionKey) {
                        case "uiInit":
                            uiInitFunction = options.functions[functionKey];
                            break;
                        default:
                            client.functions[functionKey] = options.functions[functionKey];
                    }
                }
            }
            if (options.searchCallbacks) {
                for(let callbackKey in options.searchCallbacks) {
                    client.searchCallbacks[callbackKey] = options.searchCallbacks[callbackKey];
                }
            }
            if (options.errorCallbacks) {
                for(let callbackKey in options.errorCallbacks) {
                    client.errorCallbacks[callbackKey] = options.errorCallbacks[callbackKey];
                }
            }
            if (options.cssClasses) {
                for(let cssKey in options.cssClasses) {
                    client.cssClasses[cssKey] = options.cssClasses[cssKey];
                }
            }
            if (options.locMap) {
                client._locInstance.putAll(options.locMap);
            }
            if (options.ignoreList) {
                let ignoreList = options.ignoreList;
                let ignoreArray;
                if (typeof ignoreList === "string") {
                    ignoreArray = ignoreList.split(",");
                } else if (Array.isArray(ignoreList)) {
                    ignoreArray = ignoreList;
                }
                if (ignoreArray) {
                    for(let ignore of ignoreArray) {
                        let token = ignore.trim();
                        if (token.endsWith('*')) {
                            client._ignoreArray.push({token: token.substring(0, token.length - 1), mode:"start"})
                        } else {
                            client._ignoreArray.push({token: token, mode:"equal"})
                        }  
                    }
                }
            }
            if (options.ficheBodyList) {
                let ficheBodyList = options.ficheBodyList;
                let array1;
                if (typeof ficheBodyList === "string") {
                    array1 = ficheBodyList.split(",");
                } else if (Array.isArray(ficheBodyList)) {
                    array1 = ficheBodyList;
                }
                if (array1) {
                    let array2 = new Array();
                    for(let token of array1) {
                        token = token.trim();
                        if (token.length > 0) {
                            array2.push(token);
                        }
                    }
                    if (array2.length > 0) {
                        client._ficheBodyTemplateArray = array2;
                    }
                }
            }
        }
    }
    
    function _initHelpers (client) {
        $.views.helpers({
            scrutari_client: client,
            scrutari_loc:  function(key, ...values) {
                return client.loc(key, values);
            },
            scrutari_format: function(number) {
                return client.formatNumber(number);
            },
            scrutari_mark: function (markArray) {
                return Scrutari.Utils.mark(markArray, client.cssClasses.mark);
            },
            scrutari_unmark: function (markArray) {
                return Scrutari.Utils.unmark(markArray);
            }
        });
    }
    
    function _initMaps (client) {
        __initHtmlObject(client._htmlObject);
        __initHtmlObject(options.htmlObject);
        $("script[type='text/x-scrutari-structure']").each(function (index, element) {
            client._structureMap.set(element.dataset.name, $(element).html());
        });
        $("script[type='text/x-scrutari-template']").each(function (index, element) {
            client._templateMap.set(element.dataset.name, $(element).html());
        });
        for(let entries of client._templateMap.entries()) {
            let templateText = entries[1];
            templateText = templateText.replace(/tmpl=\"([-_0-9a-z]+)\"/g, 'tmpl="scrutari:$1"');
            $.templates("scrutari:" + entries[0], templateText);
        }
        
        function __initHtmlObject (htmlObject) {
            if (htmlObject) {
                if (htmlObject.structure) {
                    for(let key in htmlObject.structure) {
                        client._structureMap.set(key, htmlObject.structure[key]);
                    }
                }
                if (htmlObject.templates) {
                    for(let key in htmlObject.templates) {
                        client._templateMap.set(key, htmlObject.templates[key]);
                    }
                }
            }
        }
    }
        
};


/**
 * 
 * @callback Scrutari.Client~callback
 * @param {Scrutari.Client} client
 * @returns {void}
 */
